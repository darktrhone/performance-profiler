﻿namespace PerformanceProfiler.Enum
{
    /// <summary>
    /// Allows you to define how CPU Samples are colected.
    /// </summary>
    public enum BenchmarkSampleTypeEnum
    {
        /// <summary> Calculate using CounterSamples. </summary>
        CounterSamples,
        /// <summary> Calculate using CounterValues. </summary>
        CounterValues,
        /// <summary> Calculate using CPU dedicated time. </summary>
        ApplicationTime,
        /// <summary> Average from all sample types. </summary>
        Mixed
    }
}