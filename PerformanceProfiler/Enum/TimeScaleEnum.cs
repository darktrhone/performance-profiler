﻿namespace PerformanceProfiler.Enum
{
    /// <summary> 
    /// Allows you to define the time scale to use.
    /// </summary>
    public enum TimeScaleEnum
    {
        Seconds,
        Miliseconds,
        Microseconds,
        Nanoseconds
    }
}