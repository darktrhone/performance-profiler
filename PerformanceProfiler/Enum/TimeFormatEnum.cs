﻿namespace PerformanceProfiler.Enum
{
    /// <summary> 
    /// Allows you to define the time output format.
    /// </summary>
    public static class TimeFormatEnum
    {
        public static string ZeroDecimalPlaces = "0";
        public static string OneDecimalPlace = "0.0";
        public static string TwoDecimalPlaces = "0.00";
        public static string ThreeDecimalPlaces = "0.000";
    }
}