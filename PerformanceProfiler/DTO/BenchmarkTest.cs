﻿using System.Collections.Generic;

namespace PerformanceProfiler.DTO
{
    internal class BenchmarkTest : BaseTest<BenchmarkResult>
    {
        public BenchmarkTest()
        {
            Results = new List<BenchmarkResult>();
        }
    }
}
