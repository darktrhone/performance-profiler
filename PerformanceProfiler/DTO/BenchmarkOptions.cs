﻿using System;
using PerformanceProfiler.Enum;

namespace PerformanceProfiler.DTO
{
    /// <summary>
    /// Allows you to customize the benchmark behaviour of the profiler.
    /// </summary>
    public class BenchmarkOptions
    {
        /// <summary>
        /// Allows you to decide how to take samples from CPU.
        /// </summary>
        public BenchmarkSampleTypeEnum BenchmarkSampleType { get; set; }
        

        /// <summary>
        /// Sets how fast we should take samples from CPU.
        /// </summary>
        public TimeSpan CpuSamplingRateInMilisseconds { get; set; }
        

        /// <summary>
        /// Sets the amount of time waiting between repetition.
        /// </summary>
        public TimeSpan ThreadWaitBetweenRepetition { get; set; }


        /// <summary>
        /// Allows you to smooth results by removing the edges.
        /// </summary>
        public bool SmoothResults { get; set; }

        /// <summary>
        /// Allows you to customize the behaviour of the profiler.
        /// </summary>
        public BenchmarkOptions()
        {
            this.BenchmarkSampleType = BenchmarkSampleTypeEnum.Mixed;

            this.CpuSamplingRateInMilisseconds = TimeSpan.FromMilliseconds(100);

            this.ThreadWaitBetweenRepetition = TimeSpan.FromMilliseconds(100);

            this.SmoothResults = true;
        }

    }
}