﻿using PerformanceProfiler.Enum;

namespace PerformanceProfiler.DTO
{
    internal class BaseResult
    {
        public string Seconds { get; set; }
        public string Miliseconds { get; set; }
        public string Microseconds { get; set; }
        public string Nanoseconds { get; set; }
        public object MethodOutputResult { get; set; }

        internal string GetFieldByEnum(TimeScaleEnum val)
        {
            switch (val)
            {
                case TimeScaleEnum.Seconds:
                    return Seconds;
                case TimeScaleEnum.Miliseconds:
                    return Miliseconds;
                case TimeScaleEnum.Microseconds:
                    return Microseconds;
                case TimeScaleEnum.Nanoseconds:
                    return Nanoseconds;
                default:
                    return "";
            }
        }

    }
}
