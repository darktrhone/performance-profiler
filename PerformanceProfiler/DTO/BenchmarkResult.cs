﻿namespace PerformanceProfiler.DTO
{
    internal class BenchmarkResult : BaseResult
    {
        public string CpuCounter { get; set; }
        public string RamCounter { get; set; }


        public BenchmarkResult()
        {

        }

    }


}
