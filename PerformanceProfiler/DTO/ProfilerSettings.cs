﻿using PerformanceProfiler.Enum;

namespace PerformanceProfiler.DTO
{
    /// <summary>
    /// Allows you to customize the behaviour of the profiler.
    /// </summary>
    public class ProfilerSettings
    {

        /// <summary>
        /// Allows you to set the amount of iterations.
        /// </summary>
        public int Iterations { get; set; }


        /// <summary>
        /// Allows you to set the amount of repetitions.
        /// </summary>
        public int Repetitions { get; set; }


        /// <summary>
        /// Allows you to set the amount of warmup repetitions.  
        /// </summary>
        public int WarmupRepetitions { get; set; }


        /// <summary>
        /// Allows you to set the time scale of the results.
        /// </summary>
        public TimeScaleEnum TimeScaleEnum { get; set; }


        /// <summary>
        /// Allows you to set the time format.
        /// </summary>
        public string TimeFormatOutput { get; set; }


        /// <summary>
        /// Allows you to set the benchmark options.
        /// </summary>
        public BenchmarkOptions BenchmarkOptions { get; set; }


        /// <summary>
        /// Allows you to customize the behaviour of the profiler.
        /// </summary>
        public ProfilerSettings()
        { 
            this.Iterations = 1;

            this.Repetitions = 1;

            this.WarmupRepetitions = 0;

            this.TimeScaleEnum = TimeScaleEnum.Miliseconds;

            this.TimeFormatOutput = "";

            this.BenchmarkOptions = new BenchmarkOptions();


        }

    }

}
