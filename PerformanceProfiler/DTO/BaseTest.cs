﻿using System;
using System.Collections.Generic;

namespace PerformanceProfiler.DTO
{
    internal class BaseTest<T> where T : class
    {
        public string MethodName { get; set; }

        public Type MethodOutputType { get; set; }

        public List<T> Results { get; set; }

        public string Avg { get; set; }

        public string Max { get; set; }

        public string Min { get; set; }

        public string Sum { get; set; }

    }
}
