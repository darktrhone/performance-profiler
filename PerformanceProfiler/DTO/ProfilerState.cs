﻿namespace PerformanceProfiler.DTO
{
    
    internal class ProfilerState
    {
        public bool Relaxing { get; set; }
        public bool IsComparing { get; set; }
        public bool IsAsserting { get; set; }
        public bool IsBenchmarking { get; set; }
        public bool IsBenchmarkComparing { get; set; }
        public bool IsTesting { get; set; }
    }
    
}