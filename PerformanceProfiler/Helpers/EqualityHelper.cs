﻿using Newtonsoft.Json;

namespace PerformanceProfiler.Helpers
{
    internal static class EqualityHelper
    {
        public static bool CheckEqualityUsingJsonSerializer(object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };
            var objJson = JsonConvert.SerializeObject(obj, microsoftDateFormatSettings);
            var anotherJson = JsonConvert.SerializeObject(another, microsoftDateFormatSettings);

            return objJson == anotherJson;
        }
    }
}
