﻿using System;

namespace PerformanceProfiler.Helpers
{
    internal static class ConsoleHelper
    {
        private static ConsoleColor GetConsoleColorByDoubleString(string val1, string val2)
        {
            if (Double.Parse(val1) > double.Parse(val2))
                return ConsoleColor.Red;
            else if (Double.Parse(val1) < double.Parse(val2))
                return ConsoleColor.Green;
            else
                return ConsoleColor.Yellow;

        }
        private static ConsoleColor GetConsoleColorByEquals(string val)
        {
            return val == "Equal" ? ConsoleColor.Green : ConsoleColor.Red;
        }

        public static void WriteCompareTable(string val1, string val2, string timeNotation, string entry, string timeFormat, bool isComparing = false)
        {
            string format1;
            string format2;
            if (isComparing)
            {
                var v1 = decimal.Parse(val1);
                var v2 = decimal.Parse(val2);

                if (v1 < v2)
                {
                    format1 = $"{val1.PadLeft(18).PadRight(2)}{timeNotation} {GetPercentage(v1, v2, timeFormat)}%";
                    format2 = $"{val2.PadLeft(18).PadRight(2)}{timeNotation}";
                }
                else
                {
                    format1 = $"{val1.PadLeft(18).PadRight(2)}{timeNotation}";
                    format2 = $"{val2.PadLeft(18).PadRight(2)}{timeNotation} {GetPercentage(v2, v1, timeFormat)}%";
                }

            }
            else
            {
                format1 = $"{val1.PadLeft(18).PadRight(2)}{timeNotation}";
                format2 = $"{val2.PadLeft(18).PadRight(2)}{timeNotation}";
            }

            Console.WriteLine("|----|--------------------------------|--------------------------------|");
            Console.Write($"|{entry.PadLeft(2),-4}|");

            Console.ForegroundColor = GetConsoleColorByDoubleString(val1, val2);
            Console.Write($"{$"{format1,-32}"}");

            Console.ResetColor();
            Console.Write("|");

            Console.ForegroundColor = GetConsoleColorByDoubleString(val2, val1);
            Console.Write($"{$"{format2,-32}"}");

            Console.ResetColor();
            Console.WriteLine("|");
        }

        private static string GetPercentage(decimal v1, decimal v2, string timeFormat)
        {
            if (v1 == v2)
                return ((double) 0).ToString(timeFormat);
            else if (v1 == decimal.Zero || v2 == decimal.Zero)
                return ((double)100).ToString(timeFormat);
            else
                return ((v2 - v1) / Math.Abs(v1) * 100).ToString(timeFormat);
        }

        public static void WriteOutputMessage(string name, string val, string timeNotation)
        {
            Console.WriteLine($"{name} = {val} {timeNotation}. ");
        }

        public static void ShowResultsAsserted(string outputType, string result, string value1, string value2)
        {
            Console.WriteLine($"Output Type : {outputType}");
            Console.WriteLine($"Output1 : {value1}");
            Console.WriteLine($"Output2 : {value2}");
            Console.Write($"Result : ");

            Console.ForegroundColor = GetConsoleColorByEquals(result);
            Console.WriteLine($"{result}\r\n");

            Console.ResetColor();
        }



    }
}
