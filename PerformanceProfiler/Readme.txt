﻿

1 - Setup our Performance Profiler.
ProfilerSettings Settings = new ProfilerSettings
{
    Iterations = 1000,
    Repetitions = 10,
    WarmupRepetitions = 2,
    TimeScaleEnum = TimeScaleEnum.Miliseconds,
    TimeFormatOutput = TimeFormatEnum.TwoDecimalPlaces,
    BenchmarkOptions = new BenchmarkOptions
    {
        SmoothResults = true,
        BenchmarkSampleType = BenchmarkSampleTypeEnum.Mixed,
        CpuSamplingRateInMilisseconds = TimeSpan.FromMilliseconds(100),
        ThreadWaitBetweenRepetition = TimeSpan.FromMilliseconds(100)
    }
};



2 - Instatiate with Settings.
PerformanceProfiler Profiler = new PerformanceProfiler(Settings);



3 - Examples:
  
    3.1 - Test
		Profiler.Test(UsingCountMethod, StaticObjects.ListOfInt);


	3.2 - Compare
		Profiler.Compare(UsingCountProperty, StaticObjects.ListOfInt).With(UsingCountMethod, StaticObjects.ListOfInt);
		

	3.3 - Assert	
        Profiler.Assert(UsingCountProperty, StaticObjects.ListOfInt).With(UsingCountMethod, StaticObjects.ListOfInt); 
		Profiler.Assert(UsingCountProperty, StaticObjects.ListOfInt).With(1500);


	3.4 - Benchmark
		Profiler.Benchmark(UsingCountProperty, StaticObjects.ListOfInt);


	3.5 - BenchmarkCompare
		Profiler.BenchmarkCompare(UsingCountProperty,StaticObjects.ListOfInt).With(UsingCountMethod, StaticObjects.ListOfInt);


 	3.6 - Type Inference  
		Profiler.Assert(ProtobufDeserialization<byte[]>, ProtobufSerialization(StaticObjects.ByteArray))
				 .With(WireDeserialization<byte[]>, WireSerialization(StaticObjects.ByteArray));

		Profiler.BenchmarkCompare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
                 .With(WireDeserialization<string>, WireSerialization(StaticObjects.QuiteLargeString));



