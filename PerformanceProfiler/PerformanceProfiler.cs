﻿#pragma warning disable CC0031 // Check for null before calling a delegate
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using PerformanceProfiler.DTO;
using PerformanceProfiler.Extensions;
using PerformanceProfiler.Helpers;

namespace PerformanceProfiler
{
    public class PerformanceProfiler
    {
        private ProfilerSettings _Settings { get; set; }
        private ProfilerState _ProfilerState { get; set; }
        private Lazy<BenchmarkProfiler> LazyBenchmarkProfiler { get; set; } = new Lazy<BenchmarkProfiler>(() => new BenchmarkProfiler());
        private BenchmarkProfiler _BenchmarkProfiler { get { return this.LazyBenchmarkProfiler.Value; } }

        private Stopwatch _Stopwatch { get; set; }
        private ProfilerTest _LastTest { get; set; }
        private ProfilerTest _CurrentTest { get; set; }

        private BenchmarkTest _LastBenchmark { get; set; }
        private BenchmarkTest _CurrentBenchmark { get; set; }




        private int GetRepetitions()
        {
            return (!_ProfilerState.IsAsserting) ? _Settings.Repetitions : 1;
        }
        private int GetIterations()
        {
            return (!_ProfilerState.IsAsserting) ? _Settings.Iterations : 1;
        }
        private int GetWarmupRepetitions()
        {
            return (!_ProfilerState.IsAsserting) ? _Settings.WarmupRepetitions : 0;
        }

        /// <summary>
        /// This library is used to profile your own code.
        /// It allows you to measure time, cpu usage and allows you to compare those results against another code.
        /// </summary>
        /// <param name="settings"></param>
        public PerformanceProfiler(ProfilerSettings settings)
        {
            this._Settings = settings;
            this._Stopwatch = new Stopwatch();
            this._ProfilerState = new ProfilerState();
        }

        #region Test

        #region Test Async Action 
        public Task TestAsync(Action methodTask)
        {
            return Task.Run(async () =>
            {
                _ProfilerState.IsTesting = true;

                // Setup
                SetupTest(methodTask);

                // Run
                await RunWarmupTestAsyncHelper(methodTask);

                await Task.Run(() => RunTestAsyncHelper(methodTask));

                // Save Test
                SaveTest();

                // Show End Results
                ShowStatisticResults();

                // Show Finishlines
                ShowFinishlines();
            });
        }
        private async Task RunWarmupTestAsyncHelper(Action methodTask)
        {
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask();
                }

                ConsoleHelper.WriteOutputMessage(_CurrentTest.MethodName, $"Warmup: {repetitions + 1}", string.Empty);

                Thread.Sleep(1);
            }
        }
        private async Task RunTestAsyncHelper(Action methodTask)
        {
            ConsoleHelper.WriteOutputMessage(_CurrentTest.MethodName, $"Initiating tests", string.Empty);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask();
                }
                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

                Thread.Sleep(1);
            }
        }

        #endregion
        #region Test Action

        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test(Action methodTask)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask();
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask();
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();
            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1>(Action<T1> methodTask, T1 arg1)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();


        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2>(Action<T1, T2> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();


        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3>(Action<T1, T2, T3> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();


        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4>(Action<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();


        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();


        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();


        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup  
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult();

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();

        }
        #endregion

        #region Test Func<T>


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T>(Func<T> methodTask)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T result = default(T);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask();
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask();
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2>(Func<T1, T2> methodTask, T1 arg1)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T2 result = default(T2);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1);
                }
            }


            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3>(Func<T1, T2, T3> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T3 result = default(T3);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2);
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4>(Func<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T4 result = default(T4);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3);
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T5 result = default(T5);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4);
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T6 result = default(T6);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5);
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles execution time for the method passed as argument.
        /// </summary>
        public void Test<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsTesting = true;

            // Setup
            SetupTest(methodTask);

            // Warmup 
            T7 result = default(T7);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }
            }

            // Start Test
            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }

                _Stopwatch.Stop();

                // Save Result
                SaveResult(result);

                // Show Current Result
                ShowResult();

            }

            // Save Test
            SaveTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }

        #endregion

        #endregion

        #region Benchmark

        #region Benchmark Func<T>

        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T>(Func<T> methodTask)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T result = default(T);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask();
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask();
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2>(Func<T1, T2> methodTask, T1 arg1)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T2 result = default(T2);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1);
                }
            }

            Thread.Sleep(1000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3>(Func<T1, T2, T3> methodTask, T1 arg1, T2 arg2)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T3 result = default(T3);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4>(Func<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T4 result = default(T4);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T5 result = default(T5);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T6 result = default(T6);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            T7 result = default(T7);
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    result = methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark(result);

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }

        #endregion

        #region Benchmark Action


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark(Action methodTask)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask();
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask();
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T>(Action<T> methodTask, T arg1)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2>(Action<T1, T2> methodTask, T1 arg1, T2 arg2)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3>(Action<T1, T2, T3> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4>(Action<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {

                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        /// <summary>
        ///  Profiles CPU Usage for the method passed as argument.
        /// </summary>
        public void Benchmark<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            // Setup
            SetupBenchmark(methodTask);

            // Warmup 
            for (var repetitions = 0; repetitions < GetWarmupRepetitions(); repetitions++)
            {
                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }
            }

            Thread.Sleep(2000);

            StartCPUSampling(_Settings.BenchmarkOptions.CpuSamplingRateInMilisseconds);

            for (var repetitions = 0; repetitions < GetRepetitions(); repetitions++)
            {
                _Stopwatch.Restart();

                for (var iterations = 0; iterations < GetIterations(); iterations++)
                {
                    methodTask(arg1, arg2, arg3, arg4, arg5, arg6);
                }

                _Stopwatch.Stop();

                Thread.Sleep(_Settings.BenchmarkOptions.ThreadWaitBetweenRepetition);

                // Save Benchmark
                SaveBenchmark();

                // Show Current Result
                ShowResult();

                // Clear Result
                _BenchmarkProfiler.BenchmarkCpuValue.Clear();

                Thread.Sleep(1);
            }

            // Disable Stopwatch
            _BenchmarkProfiler.StopwatchActive = false;

            // Save Test
            SaveBenchmarkTest();

            // Show End Results
            ShowStatisticResults();

            // Show Finishlines
            ShowFinishlines();
        }


        #endregion

        #endregion

        #region Benchmark Compare



        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare(Action methodTask)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1>(Action<T1> methodTask, T1 arg1)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2>(Action<T1, T2> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3>(Action<T1, T2, T3> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4>(Action<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3,
            T4 arg4)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3, arg4);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4, T5 arg5)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1,
            T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            return this;
        }
        

        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T>(Func<T> methodTask)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2>(Func<T1, T2> methodTask, T1 arg1)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3>(Func<T1, T2, T3> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4>(Func<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3, arg4);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1,
            T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5);
            return this;
        }


        /// <summary>
        ///  Allows you to profile CPU Usage for the argument method and compare it against another.
        /// </summary>
        public PerformanceProfiler BenchmarkCompare<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7> methodTask,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsBenchmarkComparing = true;
            Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            return this;
        }


        #endregion

        #region Compare



        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare(Action methodTask)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1>(Action<T1> methodTask, T1 arg1)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2>(Action<T1, T2> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3>(Action<T1, T2, T3> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4>(Action<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3,
            T4 arg4)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3, arg4);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4, T5 arg5)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3, arg4, arg5);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1,
            T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T>(Func<T> methodTask)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2>(Func<T1, T2> methodTask, T1 arg1)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3>(Func<T1, T2, T3> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4>(Func<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3, arg4);
            return this;
        }


        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1,
            T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3, arg4, arg5);
            return this;
        }



        /// <summary>
        ///  Allows you to compare method exection time against another.
        /// </summary>
        public PerformanceProfiler Compare<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7> methodTask,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsComparing = true;
            Test(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            return this;
        }


        #endregion

        #region Assert
        

        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T>(Func<T> methodTask)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask();

            SaveResult(methodOutput);

            return this;
        }


        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T1, T2>(Func<T1, T2> methodTask, T1 arg1)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask(arg1);

            SaveResult(methodOutput);

            return this;
        }


        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T1, T2, T3>(Func<T1, T2, T3> methodTask, T1 arg1, T2 arg2)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask(arg1, arg2);

            SaveResult(methodOutput);

            return this;
        }


        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T1, T2, T3, T4>(Func<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask(arg1, arg2, arg3);

            SaveResult(methodOutput);

            return this;
        }


        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask(arg1, arg2, arg3, arg4);

            SaveResult(methodOutput);

            return this;
        }


        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1,
            T2 arg2, T3 arg3,
            T4 arg4, T5 arg5)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask(arg1, arg2, arg3, arg4, arg5);

            SaveResult(methodOutput);

            return this;
        }


        /// <summary>
        ///  Allows you to assert method output against another.
        /// </summary>
        public PerformanceProfiler Assert<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7> methodTask,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            _ProfilerState.IsAsserting = true;

            SetupTest(methodTask);

            var methodOutput = methodTask(arg1, arg2, arg3, arg4, arg5, arg6);

            SaveResult(methodOutput);

            return this;
        }

        #endregion

        #region With



        #region With-Action


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>
        public void With(Action methodTask)
        {

            if (_ProfilerState.IsComparing)
            {
                Test(methodTask);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask);
            }

            ShowResults();

            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>
        public void With<T1>(Action<T1> methodTask, T1 arg1)
        {
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1);
            }

            ShowResults();

            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2>(Action<T1, T2> methodTask, T1 arg1, T2 arg2)
        {
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2);
            }

            ShowResults();

            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3>(Action<T1, T2, T3> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3);
            }

            ShowResults();

            DisposeResults();
            
        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4>(Action<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3, arg4);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3, arg4);
            }

            ShowResults();

            DisposeResults();
            
        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4,
            T5 arg5)
        {
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3, arg4, arg5);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5);
            }

            ShowResults();

            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1, T2 arg2, T3 arg3,
            T4 arg4, T5 arg5, T6 arg6)
        {
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            }

            ShowResults();

            DisposeResults();

        }

        #endregion

        #region With-Func<T> 



        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T>(Func<T> methodTask)
        {
            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask);
            }

            ShowResults();

            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2>(Func<T1, T2> methodTask, T1 arg1)
        {
            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask, arg1);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1);
            }

            ShowResults();
            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3>(Func<T1, T2, T3> methodTask, T1 arg1, T2 arg2)
        {
            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask, arg1, arg2);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2);
            }

            ShowResults();
            DisposeResults();
        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4>(Func<T1, T2, T3, T4> methodTask, T1 arg1, T2 arg2, T3 arg3)
        {
            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask, arg1, arg2, arg3);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3);
            }

            ShowResults();
            DisposeResults();
        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5> methodTask, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {

            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask, arg1, arg2, arg3, arg4);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3, arg4);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3, arg4);
            }

            ShowResults();
            DisposeResults();

        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6> methodTask, T1 arg1, T2 arg2, T3 arg3,
            T4 arg4, T5 arg5)
        {
            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask, arg1, arg2, arg3, arg4, arg5);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3, arg4, arg5);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5);
            }

            ShowResults();
            DisposeResults();
        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7> methodTask, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            if (_ProfilerState.IsAsserting)
            {
                Assert(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            }
            if (_ProfilerState.IsComparing)
            {
                Test(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                Benchmark(methodTask, arg1, arg2, arg3, arg4, arg5, arg6);
            }

            ShowResults();
            DisposeResults();
        }


        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        #endregion

        #region With-Obj<T> 

            
        /// <summary>
        ///  Method to call after initiating a compare.
        /// </summary>

        public void With<T>(T obj)
        {
            if (_ProfilerState.IsAsserting)
            {
                SetupTestObject(obj);

                SaveResult(obj);

                ShowResults();

                DisposeResults();

            }
        }

        #endregion


        #endregion

        private void ValidateInputs(Delegate methodTask)
        {
            if (_Settings == null)
                throw new ArgumentNullException(nameof(_Settings));

            if (methodTask == null)
                throw new ArgumentNullException(nameof(methodTask));

            if (methodTask.GetMethodInfo() == null)
                throw new NotSupportedException(nameof(methodTask));

            if (_Settings.Iterations <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_Settings.Iterations));
            }
            if (_Settings.Repetitions <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_Settings.Repetitions));
            }
            if (_ProfilerState.IsAsserting)
            {
                var currentReturnType = methodTask.GetMethodInfo().ReturnType;
                if (currentReturnType == typeof(void))
                {
                    throw new NotSupportedException($"Unable to assert void returning types. Method :: {methodTask.Method.Name}");
                }
                if (_CurrentTest?.MethodOutputType != null && currentReturnType != _CurrentTest.MethodOutputType)
                {
                    throw new NotSupportedException($"Unable to assert different returning type methods :: {_CurrentTest.MethodOutputType} :: {currentReturnType}");
                }
            }
        }

        private void ValidateInputs<T>(T obj)
        {
            if (_Settings == null)
                throw new ArgumentNullException(nameof(_Settings));

            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            if (_Settings.Iterations <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_Settings.Iterations));
            }
            if (_Settings.Repetitions <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(_Settings.Repetitions));
            }
        }

        private void ShowHeadlines()
        {
            var methodName = _ProfilerState.IsBenchmarking ? _CurrentBenchmark.MethodName : _CurrentTest.MethodName;

            if (_ProfilerState.IsAsserting)
            {
                Console.WriteLine($"Asserting {methodName}.");
                return;
            }

            string text = String.Empty;
            if (_ProfilerState.IsBenchmarking || _ProfilerState.IsBenchmarkComparing)
            {
                text = "Benchmarking";
            }
            if (_ProfilerState.IsComparing || _ProfilerState.IsTesting)
            {
                text = "Testing";
            }

            if (_Settings.WarmupRepetitions > 0)
            {
                Console.WriteLine($"{text} {methodName} with {_Settings.WarmupRepetitions} executions as warmup.\r\n");
            }
            else
            {
                Console.WriteLine($"{text} {methodName}:\r\n");
            }
        }

        private void ShowFinishlines()
        {
            if (_ProfilerState.IsBenchmarking)
            {
                Console.WriteLine($"Finished {_CurrentBenchmark.MethodName}: \r\n");
            }
            else
            if (!_ProfilerState.IsComparing)
            {
                Console.WriteLine($"Finished {_CurrentTest.MethodName}: \r\n");
            }


        }

        private void ShowResult()
        {
            if (_ProfilerState.IsBenchmarkComparing || _ProfilerState.IsComparing)
            {
                return;
            }

            if (_ProfilerState.IsBenchmarking)
            {
                ConsoleHelper.WriteOutputMessage(_CurrentBenchmark.MethodName, _CurrentBenchmark.Results.LastOrDefault().CpuCounter, "%");
            }
            else
            {
                ConsoleHelper.WriteOutputMessage(_CurrentTest.MethodName, _CurrentTest.Results.LastOrDefault().GetFieldByEnum(_Settings.TimeScaleEnum), _Settings.TimeScaleEnum.ToFriendlyString());
            }

        }

        private void ShowStatisticResults()
        {
            if (_ProfilerState.IsBenchmarkComparing || _ProfilerState.IsComparing)
            {
                return;
            }

            var timeNotation = _Settings.TimeScaleEnum.ToFriendlyString();

            if (_ProfilerState.IsBenchmarking)
            {
                Console.WriteLine();
                ConsoleHelper.WriteOutputMessage("Max", _CurrentBenchmark.Max, "%");
                ConsoleHelper.WriteOutputMessage("Min", _CurrentBenchmark.Min, "%");
                ConsoleHelper.WriteOutputMessage("Avg", _CurrentBenchmark.Avg, "%");
                ConsoleHelper.WriteOutputMessage("Time", _CurrentBenchmark.Sum, timeNotation);
            }
            else
            {
                Console.WriteLine();
                ConsoleHelper.WriteOutputMessage("Max", _CurrentTest.Max, timeNotation);
                ConsoleHelper.WriteOutputMessage("Min", _CurrentTest.Min, timeNotation);
                ConsoleHelper.WriteOutputMessage("Avg", _CurrentTest.Avg, timeNotation);
                ConsoleHelper.WriteOutputMessage("Sum", _CurrentTest.Sum, timeNotation);
            }
        }

        private void ShowResultsBenchmarked()
        {
            string name1 = "     " + _LastBenchmark.MethodName;
            while (name1.Length < 39)
            {
                name1 = " " + name1 + " ";
            }

            string name2 = _CurrentBenchmark.MethodName;
            while (name2.Length < 30)
            {
                name2 = " " + name2 + " ";
            }


            Console.WriteLine($"\r\n{name1} {name2}");

            for (var i = 0; i < _Settings.Repetitions; i++)
            {
                var timeFirst = _LastBenchmark.Results[i].CpuCounter;
                var timeSecond = _CurrentBenchmark.Results[i].CpuCounter;
                ConsoleHelper.WriteCompareTable(timeFirst, timeSecond, "%", (i + 1).ToString(), _Settings.TimeFormatOutput);
            }

            ConsoleHelper.WriteCompareTable(_LastBenchmark.Max, _CurrentBenchmark.Max, "%", "Max", _Settings.TimeFormatOutput, true);
            ConsoleHelper.WriteCompareTable(_LastBenchmark.Min, _CurrentBenchmark.Min, "%", "Min", _Settings.TimeFormatOutput, true);
            ConsoleHelper.WriteCompareTable(_LastBenchmark.Avg, _CurrentBenchmark.Avg, "%", "Avg", _Settings.TimeFormatOutput, true);
            ConsoleHelper.WriteCompareTable(_LastBenchmark.Sum, _CurrentBenchmark.Sum, _Settings.TimeScaleEnum.ToFriendlyString(), "Time", _Settings.TimeFormatOutput, true);

            Console.ResetColor();
            Console.WriteLine("|----|--------------------------------|--------------------------------|");


        }
        private void ShowResultsCompared()
        {
            string name1 = "     " + _LastTest.MethodName;
            while (name1.Length < 39)
            {
                name1 = " " + name1 + " ";
            }

            string name2 = _CurrentTest.MethodName;
            while (name2.Length < 30)
            {
                name2 = " " + name2 + " ";
            }


            Console.WriteLine($"\r\n{name1} {name2}");
            var timeNotation = _Settings.TimeScaleEnum.ToFriendlyString();

            for (var i = 0; i < _Settings.Repetitions; i++)
            {
                var timeFirst = _LastTest.Results[i].GetFieldByEnum(_Settings.TimeScaleEnum);
                var timeSecond = _CurrentTest.Results[i].GetFieldByEnum(_Settings.TimeScaleEnum);
                ConsoleHelper.WriteCompareTable(timeFirst, timeSecond, timeNotation, (i + 1).ToString(), _Settings.TimeFormatOutput);
            }

            ConsoleHelper.WriteCompareTable(_LastTest.Max, _CurrentTest.Max, timeNotation, "Max", _Settings.TimeFormatOutput, true);
            ConsoleHelper.WriteCompareTable(_LastTest.Min, _CurrentTest.Min, timeNotation, "Min", _Settings.TimeFormatOutput, true);
            ConsoleHelper.WriteCompareTable(_LastTest.Avg, _CurrentTest.Avg, timeNotation, "Avg", _Settings.TimeFormatOutput, true);
            ConsoleHelper.WriteCompareTable(_LastTest.Sum, _CurrentTest.Sum, timeNotation, "Sum", _Settings.TimeFormatOutput, true);


            Console.ResetColor();
            Console.WriteLine("|----|--------------------------------|--------------------------------|");

        }

        private void ShowResults()
        {
            if (_ProfilerState.IsAsserting)
            {
                ShowResultsAsserted();
                return;
            }
            if (_ProfilerState.IsBenchmarkComparing)
            {
                ShowResultsBenchmarked();
                return;
            }
            if (_ProfilerState.IsComparing)
            {
                ShowResultsCompared();
                return;
            }
        }

        private void ShowResultsAsserted()
        {
            var res1 = _LastTest.Results.First().MethodOutputResult;
            var res2 = _CurrentTest.Results.First().MethodOutputResult;
            bool result = EqualityHelper.CheckEqualityUsingJsonSerializer(res1, res2);

            var message = result ? "Equal" : "Not Equal";
            var res1Output = res1 != null ? res1.ToString() : "null";
            var res2Output = res2 != null ? res2.ToString() : "null";
            ConsoleHelper.ShowResultsAsserted(_LastTest.MethodOutputType.ToString(), message, res1Output, res2Output);
        }


        private void SaveResult()
        {
            var result = new ProfilerTestResult
            {
                Seconds = GetElapsedSeconds(),
                Miliseconds = GetElapsedMilisseconds(),
                Microseconds = GetElapsedMicroseconds(),
                Nanoseconds = GetElapsedNanoseconds()
            };

            _CurrentTest.Results.Add(result);
        }

        private void SaveResult<T>(T output)
        {
            var result = new ProfilerTestResult
            {
                Seconds = GetElapsedSeconds(),
                Miliseconds = GetElapsedMilisseconds(),
                Microseconds = GetElapsedMicroseconds(),
                Nanoseconds = GetElapsedNanoseconds(),
                MethodOutputResult = output,
            };

            _CurrentTest.Results.Add(result);
        }


        private void SaveBenchmark()
        {
            if (_Settings.BenchmarkOptions.SmoothResults)
            {
                var count = _BenchmarkProfiler.BenchmarkCpuValue.Count;
                var edges = count / 4;

                _BenchmarkProfiler.BenchmarkCpuValue = _BenchmarkProfiler.BenchmarkCpuValue.OrderBy(x => x).Skip(edges).Take(count - edges * 2).ToList();
            }

            var result = new BenchmarkResult
            {
                Seconds = GetElapsedSeconds(),
                Miliseconds = GetElapsedMilisseconds(),
                Microseconds = GetElapsedMicroseconds(),
                Nanoseconds = GetElapsedNanoseconds(),
                CpuCounter = _BenchmarkProfiler.BenchmarkCpuValue.GetAverageFormatted(_Settings.TimeFormatOutput),
                //  RamCounter = ((RamCounterSum.Where(x => x > 0).Average()) / 100 / 1024 / 1024).ToString("0.00"),
            };

            _CurrentBenchmark.Results.Add(result);
        }
        private void SaveBenchmark<T>(T output)
        {
            if (_Settings.BenchmarkOptions.SmoothResults)
            {
                var count = _BenchmarkProfiler.BenchmarkCpuValue.Count;
                var edges = count / 4;

                _BenchmarkProfiler.BenchmarkCpuValue = _BenchmarkProfiler.BenchmarkCpuValue.OrderBy(x => x).Skip(edges).Take(count - edges * 2).ToList();
            }

            var result = new BenchmarkResult
            {
                Seconds = GetElapsedSeconds(),
                Miliseconds = GetElapsedMilisseconds(),
                Microseconds = GetElapsedMicroseconds(),
                Nanoseconds = GetElapsedNanoseconds(),
                CpuCounter = _BenchmarkProfiler.BenchmarkCpuValue.GetAverageFormatted(_Settings.TimeFormatOutput),
                //  RamCounter = ((RamCounterSum.Where(x => x > 0).Average()) / 100 / 1024 / 1024).ToString("0.00"),
            };

            _CurrentBenchmark.Results.Add(result);
        }

        private void SaveTest()
        {
            if (_CurrentTest.Results.Count == 0)
                return;

            _CurrentTest.Max = _CurrentTest.Results.Select(x => double.Parse(x.GetFieldByEnum(_Settings.TimeScaleEnum)))
                .Max()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

            _CurrentTest.Min = _CurrentTest.Results.Select(x => double.Parse(x.GetFieldByEnum(_Settings.TimeScaleEnum)))
                .Min()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

            _CurrentTest.Avg = _CurrentTest.Results.Select(x => double.Parse(x.GetFieldByEnum(_Settings.TimeScaleEnum)))
                .Average()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

            _CurrentTest.Sum = _CurrentTest.Results.Select(x => double.Parse(x.GetFieldByEnum(_Settings.TimeScaleEnum)))
                .Sum()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

        }
        private void SaveBenchmarkTest()
        {
            _CurrentBenchmark.Max = _CurrentBenchmark.Results.Select(x => Double.Parse(x.CpuCounter))
                .Max()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

            _CurrentBenchmark.Min = _CurrentBenchmark.Results.Select(x => Double.Parse(x.CpuCounter))
                .Min()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

            _CurrentBenchmark.Avg = _CurrentBenchmark.Results.Select(x => Double.Parse(x.CpuCounter))
                .Average()
                .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

            _CurrentBenchmark.Sum = _CurrentBenchmark.Results.Select(x => double.Parse(x.GetFieldByEnum(_Settings.TimeScaleEnum)))
    .Sum()
    .ToStringOrZeroWithFormat(_Settings.TimeFormatOutput);

        }

        private void CleanupOldResults()
        {
            _LastTest = _CurrentTest;
            _CurrentTest = new ProfilerTest();
            _LastBenchmark = _CurrentBenchmark;
            _CurrentBenchmark = new BenchmarkTest();
        }

        private void DisposeResults()
        {
            _LastTest = null;
            _CurrentTest = null;
            _LastBenchmark = null;
            _CurrentBenchmark = null;
            _ProfilerState.IsAsserting = false;
            _ProfilerState.IsBenchmarking = false;
            _ProfilerState.IsBenchmarkComparing = false;
            _ProfilerState.IsTesting = false;
            _ProfilerState.IsComparing = false;

        }

        private void SetupTest(Delegate methodTaskDelegate)
        {
            //Validate
            ValidateInputs(methodTaskDelegate);

            // Clean Old Results
            CleanupOldResults();

            // Set Current Method Name
            _CurrentTest.MethodName = methodTaskDelegate.GetMethodInfo().Name;

            // Set Current Method Return Type
            _CurrentTest.MethodOutputType = methodTaskDelegate.GetMethodInfo().ReturnType;

            // Show Headlines
            Task.Run(() => ShowHeadlines());

            // Clean GC
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        private void SetupBenchmark(Delegate methodTaskDelegate)
        {
            //Validate
            ValidateInputs(methodTaskDelegate);

            // Clean Old Results
            CleanupOldResults();

            // Set Current Method Name
            _CurrentBenchmark.MethodName = methodTaskDelegate.GetMethodInfo().Name;

            // Set Current Method Return Type
            _CurrentBenchmark.MethodOutputType = methodTaskDelegate.GetMethodInfo().ReturnType;

            // Enable Stopwatch
            _BenchmarkProfiler.StopwatchActive = true;

            // Set Task
            _ProfilerState.IsBenchmarking = true;

            // Show Headlines 
            Task.Run(() => ShowHeadlines());

            // Clean GC
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        private void SetupTestObject<T>(T obj)
        {
            //Validate
            ValidateInputs(obj);

            // Clean Old Results
            CleanupOldResults();

            // Set Current Method Name
            _CurrentTest.MethodName = typeof(T).Name;

            // Set Current Method Return Type
            _CurrentTest.MethodOutputType = typeof(T);

            // Show Headlines
            Task.Run(() => ShowHeadlines());

            // Clean GC
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }


        private async Task StartCPUSampling(TimeSpan interval)
        {
            while (_BenchmarkProfiler.StopwatchActive)
            {
                _BenchmarkProfiler.CalculateCpuSample(_Settings.BenchmarkOptions.BenchmarkSampleType);

                await Task.Delay(interval);
            }
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private string GetElapsedSeconds()
        {
            return ((double)_Stopwatch.ElapsedMilliseconds / 1000).ToString(_Settings.TimeFormatOutput);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private string GetElapsedMilisseconds()
        {
            return (1000 * (double)_Stopwatch.ElapsedTicks / Stopwatch.Frequency).ToString(_Settings.TimeFormatOutput);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private string GetElapsedMicroseconds()
        {
            return (1000000 * (double)_Stopwatch.ElapsedTicks / Stopwatch.Frequency).ToString(_Settings.TimeFormatOutput);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private string GetElapsedNanoseconds()
        {
            return (1000000000 * (double)_Stopwatch.ElapsedTicks / Stopwatch.Frequency).ToString(_Settings.TimeFormatOutput);
        }

    }
}
#pragma warning restore CC0031 // Check for null before calling a delegate
