﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using PerformanceProfiler.Enum;

namespace PerformanceProfiler
{
    internal class BenchmarkProfiler
    {
        public DateTime LastTime { get; set; }

        public TimeSpan LastTotalProcessorTime { get; set; }

        public DateTime CurrentTime { get; set; }

        public TimeSpan CurrentTotalProcessorTime { get; set; }

        public CounterSample LastSample { get; set; }

        public Process CurrentProcess { get; set; }

        public List<float> BenchmarkCpuValue { get; set; }
        public List<float> BenchmarkRamValue { get; set; }

        public PerformanceCounter ProcessorTimeCounter { get; set; }
        public PerformanceCounter ProcessorTotalTimeCounter { get; set; }
        public PerformanceCounter MemoryUsage { get; set; }

        public bool StopwatchActive { get; set; }

        public BenchmarkProfiler()
        {
            var currentProcessName = Process.GetCurrentProcess().ProcessName;

            this.ProcessorTimeCounter = new PerformanceCounter("Process", "% Processor Time", currentProcessName);
            this.ProcessorTotalTimeCounter = new PerformanceCounter("Process", "% Processor Time", "_Total");
            this.MemoryUsage = new PerformanceCounter("Process", "Working Set", "_Total");
            this.CurrentProcess = Process.GetProcessesByName(currentProcessName)[0];

            this.BenchmarkCpuValue = new List<float>();
            this.BenchmarkRamValue = new List<float>();

            this.LastTime = DateTime.MinValue;
            this.CurrentTime = DateTime.MinValue;
            this.LastTotalProcessorTime = TimeSpan.Zero;
            this.CurrentTotalProcessorTime = TimeSpan.Zero;

        }

        public void CalculateCpuSample(BenchmarkSampleTypeEnum BenchmarkType)
        {
            switch (BenchmarkType)
            {
                case BenchmarkSampleTypeEnum.CounterSamples:
                    {
                        var value = GetCpuByCounterSample();
                        if (value > 0 && value < 100)
                        {
                            this.BenchmarkCpuValue.Add(value);
                        }
                        break;
                    }
                case BenchmarkSampleTypeEnum.CounterValues:
                    {
                        var value = GetCpuByCounterValue();
                        if (value > 0 && value < 100)
                        {
                            this.BenchmarkCpuValue.Add(value);
                        }
                        break;
                    }

                case BenchmarkSampleTypeEnum.ApplicationTime:
                    {
                        var value = GetCpuByApplicationTime();
                        if (value > 0 && value < 100)
                        {
                            this.BenchmarkCpuValue.Add(value);
                        }
                        break;
                    }

                case BenchmarkSampleTypeEnum.Mixed:
                    {
                        float total = 0;
                        int count = 0;
                        var v1 = GetCpuByCounterSample();
                        var v2 = GetCpuByApplicationTime();
                        var v3 = GetCpuByCounterValue();

                        if (v1 > 0)
                        {
                            total += v1;
                            count++;
                        }
                        if (v2 > 0)
                        {
                            total += v2;
                            count++;
                        }
                        if (v3 > 0)
                        {
                            total += v3;
                            count++;
                        }

                        total = total / count;

                        if (total > 0 && total < 100)
                        {
                            this.BenchmarkCpuValue.Add(total);
                        }
                        break;
                    }

                default: break;
            }

        }

        private float GetCpuByCounterSample()
        {
            CounterSample counterSample = CounterSample.Empty;

            if (this.BenchmarkCpuValue.Count == 0)
            {
                this.LastSample = this.ProcessorTimeCounter.NextSample();
                Thread.Sleep(100);
            }

            counterSample = this.ProcessorTimeCounter.NextSample();

            var computedValue = CounterSampleCalculator.ComputeCounterValue(this.LastSample, counterSample) /
                                Environment.ProcessorCount;

            this.LastSample = counterSample;

            return computedValue;
        }

        private float GetCpuByCounterValue()
        {
            return this.ProcessorTimeCounter.NextValue() / Environment.ProcessorCount;
        }

        private float GetCpuByApplicationTime()
        {
            if (this.LastTime == DateTime.MinValue)
            {
                this.LastTime = DateTime.Now;
                this.LastTotalProcessorTime = CurrentProcess.TotalProcessorTime;
                return 0;
            }

            this.CurrentTime = DateTime.Now;
            this.CurrentTotalProcessorTime = CurrentProcess.TotalProcessorTime;

            float CPUUsage = (float)(CurrentTotalProcessorTime.Ticks - LastTotalProcessorTime.Ticks) / CurrentTime.Subtract(LastTime).Ticks;
            CPUUsage *= (float)(100.00 / Environment.ProcessorCount);

            this.LastTime = CurrentTime;
            this.LastTotalProcessorTime = CurrentTotalProcessorTime;

            return CPUUsage;

        }

    }
}