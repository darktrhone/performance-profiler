﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using PerformanceProfiler.Enum;

namespace PerformanceProfiler.Extensions
{
    internal static class ContentExtensions
    {
        public static string ToFriendlyString(this TimeScaleEnum enumVal)
        {
            switch (enumVal)
            {
                case TimeScaleEnum.Seconds:
                    return "s";
                case TimeScaleEnum.Miliseconds:
                    return "ms";
                case TimeScaleEnum.Microseconds:
                    return "μs";
                case TimeScaleEnum.Nanoseconds:
                    return "ns";
                default:
                    return "";
            }
        }
        public static string ToStringOrZeroWithFormat(this double str, string format)
        {
            return ToStringOrZeroWithFormat(str.ToString(CultureInfo.InvariantCulture), format);
        }
        public static string ToStringOrZeroWithFormat(this string str, string format)
        {
            return string.IsNullOrEmpty(str) ? 0.ToString(format) : double.Parse(str,CultureInfo.InvariantCulture).ToString(format);
        }
        public static string ToStringOrZeroWithFormat(this float str, string format)
        {
            return ToStringOrZeroWithFormat(str.ToString(CultureInfo.InvariantCulture),format);
        }
        public static string GetAverageFormatted(this List<float> list, string format)
        {
            return (list?.Count > 0) ? list.Average().ToStringOrZeroWithFormat(format) : "0".ToStringOrZeroWithFormat(format);
        }
    }

}
