﻿using System.Collections.Concurrent;
using System.Collections.Specialized;
using System.Configuration;

namespace PerformancePlayground.TestObjects
{
    static class StaticConfigurationManager
    {
        private static readonly ConcurrentDictionary<string, string> AppSettings;


        static StaticConfigurationManager()
        {
            AppSettings = new ConcurrentDictionary<string, string>();
            //LoadKeys();
        }

        public static string TryGetValue(string key)
        {
            string val;
            AppSettings.TryGetValue(key, out val);
            return val;
        }

        private static void LoadKeys()
        {
            var section = (NameValueCollection)ConfigurationManager.GetSection("appSettings");
            foreach (string key in section.Keys)
            {
                AppSettings.TryAdd(key, section[key]);
            }
        }
    }
}
