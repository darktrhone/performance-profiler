﻿using System;
using ProtoBuf;
using ZeroFormatter;

namespace PerformancePlayground.TestObjects
{
    [ZeroFormattable]
    [ProtoContract]
    [Serializable]
    public class SerializableProfilerSettings
    {
        [Index(0)]
        public virtual bool ShowAverage { get; set; }
        [Index(1)]
        public virtual bool ShowSum { get; set; }
        [Index(2)]
        public virtual bool ShowMax { get; set; }
        [Index(3)]
        public virtual bool ShowMin { get; set; }
        [Index(4)]
        public virtual bool ShowHeadlines { get; set; }
        [Index(5)]
        public virtual bool ShowFinishlines { get; set; }
        [Index(6)]
        public virtual int Iterations { get; set; }
        [Index(7)]
        public virtual int Repetitions { get; set; }
        [Index(8)]
        public virtual int WarmupRepetitions { get; set; }

        public void PerformanceTestOptions()
        {
            this.ShowAverage = true;

            this.ShowMax = true;

            this.ShowSum = true;

            this.ShowMin = true;

            this.ShowHeadlines = true;

            this.ShowFinishlines = true;

            this.Iterations = 1;

            this.Repetitions = 1;

            this.WarmupRepetitions = 0;


        }

    }

}
