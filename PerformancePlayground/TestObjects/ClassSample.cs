﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PerformancePlayground.TestObjects
{
    public class ClassSample
    {
        public string Value { get; set; }

        public string AnotherValue { get; set; }
    }
}
