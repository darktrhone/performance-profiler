﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PerformancePlayground.TestObjects;
using PerformanceProfiler.DTO;
using PerformanceProfiler.Enum;
using PerformancePlayground.Tests.Collections;
using PerformancePlayground.Tests.Async;
using PerformancePlayground.Tests.String;
using PerformancePlayground.Tests.Count;
using PerformancePlayground.Tests.Serialization;
using static PerformancePlayground.Program;
using PerformancePlayground.Tests.Lazy;

namespace PerformancePlayground
{
    using PerformancePlayground.Tests.Tuples;

    public static class Program
    {
        public static Random r = new Random();
        public static PerformanceProfiler.PerformanceProfiler Profiler;

        static async Task Main(string[] args)
        {
            // Consider using Release Build for more accurate results //

            ConfigureProfiler();
            
            EnumerableTests.Run();
            
            //TestListCeationWithSize();

            //testc();


            //LazyTests.Run();

            //AsyncTests.Run();

            //CountTests.Run();

            //DictionaryTests.Run();


            //SerializationTests.Run();

            // ExtensionInstanceTest.Run();
            //
            // ExtensionInstanceTest.Run();
            
            //ListTests.Run();
            ListTests.Run();
            
           // StringTests.Run();

            ////await Task.Run(() => Profiler.TestAsync(() => { string abc = ""; abc += "stuff"; string x = abc + "asd"; }));

            ////TestTuples();

            //TestGcCollect();

            Console.WriteLine("\r\nEnd Perf Test");

            Console.ReadLine();
        }

        private static void ConfigureProfiler()
        {
            //Setup our Performance Profiler
            ProfilerSettings Settings = new ProfilerSettings
            {
                Iterations = 100_000_0,
                Repetitions = 10,
                WarmupRepetitions = 2,
                TimeScaleEnum = TimeScaleEnum.Miliseconds,
                TimeFormatOutput = TimeFormatEnum.TwoDecimalPlaces,
                BenchmarkOptions = new BenchmarkOptions
                {
                    SmoothResults = true,
                    BenchmarkSampleType = BenchmarkSampleTypeEnum.Mixed,
                    CpuSamplingRateInMilisseconds = TimeSpan.FromMilliseconds(100),
                    ThreadWaitBetweenRepetition = TimeSpan.FromMilliseconds(100)
                }
            };

            // instantiate with settings
            Profiler = new PerformanceProfiler.PerformanceProfiler(Settings);
        }

        private static object UsingNew()
        {
            return new ClassSample();
        }

        private static object UsingActivator()
        {
            return Activator.CreateInstance(typeof(ClassSample));
        }



        private static object ListWithoutSize()
        {
            var sizedEmptyList = new List<ProfilerSettings>();
            return sizedEmptyList;
        }


        private static object ListWithSize()
        {
            var size = r.Next(500, 5000);
            var sizedEmptyList = new List<ProfilerSettings>(size);
            sizedEmptyList.AddRange(Enumerable.Repeat(new ProfilerSettings(), size));
            return sizedEmptyList;
        }


        private static void TestListCeationWithSize()
        {
            Profiler.Compare(ListWithSize).With(ListWithoutSize);
        }




        private static void testc()
        {
            Profiler.Compare(UsingNew).With(UsingActivator);
        }


        private static void TestGcCollect()
        {

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //Console.WriteLine("Before Total available memory before collection: {0:N0}", System.GC.GetTotalMemory(false)); ;

            //Profiler.Benchmark(BenchmarkStringConcat);

            //Console.WriteLine("After Total available memory before collection: {0:N0}", System.GC.GetTotalMemory(false));

            //GC.Collect();
            //GC.WaitForPendingFinalizers();


            //Console.WriteLine("After clean: {0:N0}", System.GC.GetTotalMemory(false));
        }


        #region Test Profiler Methods



        private static void TestProfilerMethods()
        {
            //RunAsserts();

            //RunCompares();

            //RunTests();

            //RunBenchmarks();
        }





        #endregion



        #region CacheConfigs

        private static void TestCacheTimeoutWithNewWebConfig()
        {
            Profiler.Compare(ReadConfigurationSettingsForArea).With(NewReadConfigurationSettingsForArea);

            //Profiler.Compare(SingleReadConfiguration).With(NewSingleReadConfiguration);

            //Profiler.Compare(SingleCacheReadConfiguration).With(NewSingleCacheReadConfiguration);

            //Profiler.Compare(GetCacheTimeout).With(NewGetCacheTimeout);

        }


        public static void ReadConfigurationSettingsForArea()
        {
            var controllerName = "Canal"; // this.AreaInfo.AreaControllerName;

            var CacheTimeOut = ReadCacheConfigurationForArea("CacheTimeOut", controllerName, "CanalGenericoCacheTimeOut");
            var MoreContentsColumn = ReadConfigurationForArea("MoreContentsColumn", controllerName, "CanalGenericoMoreContentsColumn");
            var MoreContentsWidget = ReadConfigurationForArea("MoreContentsWidget", controllerName, "CanalGenericoMoreContentsWidget");
            var MoreContentsNumberOfResults = int.Parse(ReadConfigurationForArea("MoreContentsNumberOfResults", controllerName, "CanalGenericoMoreContentsNumberOfResults"));
            var MaisComentadasPartilhadasColumn = ReadConfigurationForArea("MaisComentadasPartilhadasColumn", controllerName, "MaisComentadasPartilhadasColumn");
            var MaisComentadasWidget = ReadConfigurationForArea("MaisComentadasWidget", controllerName, "MaisComentadasWidget");
            var MaisPartilhadasWidget = ReadConfigurationForArea("MaisPartilhadasWidget", controllerName, "MaisPartilhadasWidget");
        }
        public static void NewReadConfigurationSettingsForArea()
        {
            var controllerName = "Canal"; // this.AreaInfo.AreaControllerName;

            var CacheTimeOut = NewReadCacheConfigurationForArea("CacheTimeOut", controllerName, "CanalGenericoCacheTimeOut");
            var MoreContentsColumn = NewReadConfigurationForArea("MoreContentsColumn", controllerName, "CanalGenericoMoreContentsColumn");
            var MoreContentsWidget = NewReadConfigurationForArea("MoreContentsWidget", controllerName, "CanalGenericoMoreContentsWidget");
            var MoreContentsNumberOfResults = int.Parse(NewReadConfigurationForArea("MoreContentsNumberOfResults", controllerName, "CanalGenericoMoreContentsNumberOfResults"));
            var MaisComentadasPartilhadasColumn = NewReadConfigurationForArea("MaisComentadasPartilhadasColumn", controllerName, "MaisComentadasPartilhadasColumn");
            var MaisComentadasWidget = NewReadConfigurationForArea("MaisComentadasWidget", controllerName, "MaisComentadasWidget");
            var MaisPartilhadasWidget = NewReadConfigurationForArea("MaisPartilhadasWidget", controllerName, "MaisPartilhadasWidget");
        }

        public static int SingleCacheReadConfiguration()
        {
            return ReadCacheConfigurationForArea("CacheTimeOut", "Canal", "CanalCacheTimeOut");
        }
        public static int NewSingleCacheReadConfiguration()
        {
            return NewReadCacheConfigurationForArea("CacheTimeOut", "Canal", "CanalCacheTimeOut");
        }

        public static int GetCacheTimeout()
        {
            return CacheTimeoutManager.GetCacheTimeout("CanalCacheTimeoutInMinutes");

        }
        public static int NewGetCacheTimeout()
        {
            return CacheTimeoutManager.NewGetCacheTimeout("CanalCacheTimeoutInMinutes", 1);

        }


        public static string SingleReadConfiguration()
        {
            return ReadConfigurationForArea("MoreContentsColumn", "Canal", "CanalGenericoMoreContentsColumn");

        }
        public static string NewSingleReadConfiguration()
        {
            return NewReadConfigurationForArea("MoreContentsColumn", "Canal", "CanalGenericoMoreContentsColumn");
        }



        private static string ReadConfigurationForArea(string keySuffix, string areaPrefix, string defaultKey)
        {
            var value = ConfigurationManager.AppSettings[string.Format("{0}{1}", areaPrefix, keySuffix)];
            if (value != null)
                return value;
            return ConfigurationManager.AppSettings[defaultKey];
        }
        private static string NewReadConfigurationForArea(string keySuffix, string areaPrefix, string defaultKey)
        {
            var value = StaticConfigurationManager.TryGetValue(areaPrefix + keySuffix);
            if (value != null)
                return value;
            return StaticConfigurationManager.TryGetValue(defaultKey);
        }

        private static int ReadCacheConfigurationForArea(string keySuffix, string areaPrefix, string defaultKey)
        {
            var key = string.Format("{0}{1}", areaPrefix, keySuffix);
            var value = ConfigurationManager.AppSettings[key];
            if (value != null)
                return CacheTimeoutManager.GetCacheTimeout(key);
            return CacheTimeoutManager.GetCacheTimeout(defaultKey);
        }
        private static int NewReadCacheConfigurationForArea(string keySuffix, string areaPrefix, string defaultKey)
        {
            int value;
            var key = areaPrefix + keySuffix;

            if (!int.TryParse(StaticConfigurationManager.TryGetValue(key), out value))
                return CacheTimeoutManager.NewGetCacheTimeout(key, value);


            return CacheTimeoutManager.NewGetCacheTimeout(defaultKey);
        }

        public static class CacheTimeoutManager
        {
            public static int GetCacheTimeout(string cacheConfigKey)
            {
                var cacheSpecialStartTimespan = ConfigurationManager.AppSettings["CacheSpecialStartTimespan"];
                var cacheSpecialEndTimespan = ConfigurationManager.AppSettings["CacheSpecialEndTimespan"];

                TimeSpan startExtraCache, endExtraCache;

                if (string.IsNullOrWhiteSpace(cacheSpecialStartTimespan)
                    || string.IsNullOrWhiteSpace(cacheSpecialEndTimespan)
                    || !TimeSpan.TryParse(cacheSpecialStartTimespan, out startExtraCache)
                    || !TimeSpan.TryParse(cacheSpecialEndTimespan, out endExtraCache))
                {
                    return GetCacheKeyFromConfig(cacheConfigKey);
                }


                var currentTimespan = DateTime.Now.TimeOfDay;
                if (currentTimespan >= startExtraCache && currentTimespan <= endExtraCache)
                {
                    return (int)(endExtraCache - currentTimespan).TotalMinutes;
                }

                return GetCacheKeyFromConfig(cacheConfigKey);
            }
            public static int NewGetCacheTimeout(string cacheConfigKey, int value = 0)
            {
                var cacheSpecialStartTimespan = StaticConfigurationManager.TryGetValue("CacheSpecialStartTimespan");
                var cacheSpecialEndTimespan = StaticConfigurationManager.TryGetValue("CacheSpecialEndTimespan");

                TimeSpan startExtraCache, endExtraCache;

                if (!TimeSpan.TryParse(cacheSpecialStartTimespan, out startExtraCache) || !TimeSpan.TryParse(cacheSpecialEndTimespan, out endExtraCache))
                {
                    return (value > 0) ? value : NewGetCacheKeyFromConfig(cacheConfigKey);
                }


                var currentTimespan = DateTime.Now.TimeOfDay;
                if (currentTimespan >= startExtraCache && currentTimespan <= endExtraCache)
                {
                    return (int)(endExtraCache - currentTimespan).TotalMinutes;
                }

                return (value > 0) ? value : NewGetCacheKeyFromConfig(cacheConfigKey);
            }
            private static int GetCacheKeyFromConfig(string cacheConfigKey)
            {
                string sCacheTime = ConfigurationManager.AppSettings[cacheConfigKey];
                int cacheTime = 0;

                if (int.TryParse(sCacheTime, out cacheTime))
                {
                    return cacheTime;
                }

                return int.Parse(ConfigurationManager.AppSettings["CacheTimeoutInMinutes"]);
            }
            private static int NewGetCacheKeyFromConfig(string cacheConfigKey)
            {
                string sCacheTime = StaticConfigurationManager.TryGetValue(cacheConfigKey);
                int cacheTime = 0;

                if (int.TryParse(sCacheTime, out cacheTime))
                {
                    return cacheTime;
                }

                return int.Parse(StaticConfigurationManager.TryGetValue("CacheTimeoutInMinutes"));
            }
        }
        #endregion

        #region Url Cleanup

        private static void TestCleanup()
        {
            string url = "sabado.stg.online.xl.pt/cultura-gps/detalhe\"\"/roger-ballen-na-barbado-em_lisboa?||ref=SEC_Destaques_cultura-gps";


            Profiler.Assert(TestAggregateClean, url).With(TestCyclicUrlClean, url);
            Profiler.Compare(TestAggregateClean, url).With(TestCyclicUrlClean, url);

            //Profiler.Compare(UrlEndsWithLegacyExtensions, StaticObjects.SimpleUrl).With(UrlEndsWithNewV3LegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Compare(CleanUrlFromLegacyExtensions, StaticObjects.SimpleUrl).With(CleanUrlV3FromNewLegacyExtensions, StaticObjects.SimpleUrl);


            //Profiler.Test(UrlEndsWithNewV3LegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Test(UrlEndsWithNewV2LegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Test(UrlEndsWithNewLegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Test(UrlEndsWithLegacyExtensions, StaticObjects.SimpleUrl);

            //Profiler.Test(CleanUrlV3FromNewLegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Test(CleanUrlV2FromNewLegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Test(CleanUrlFromNewLegacyExtensions, StaticObjects.SimpleUrl);
            //Profiler.Test(CleanUrlFromLegacyExtensions, StaticObjects.SimpleUrl);

        }

        private static string TestAggregateClean(string rawUrl)
        {
            return Path.GetInvalidPathChars().Aggregate(rawUrl, (current, c) => current.Replace(c.ToString(), string.Empty));
        }
        private static string TestCyclicUrlClean(string rawUrl)
        {
            var InvalidPathCharactersString = Path.GetInvalidPathChars().Select(c => c.ToString()).ToArray();
            int i = -1;
            while (i++ != InvalidPathCharactersString.Length - 1)
            {
                rawUrl = rawUrl.Replace(InvalidPathCharactersString[i], string.Empty);
            }

            return rawUrl;
        }

        private static bool UrlEndsWithNewV3LegacyExtensions(string url)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];
                if (url.Length >= item.Length && url.Substring(url.Length - item.Length) == item)
                {
                    return true;
                }
            }
            return false;
        }
        private static bool UrlEndsWithNewV2LegacyExtensions(string url)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                if (url.Substring(url.Length - StaticObjects.supportedLegacyExtenstion[x].Length) == StaticObjects.supportedLegacyExtenstion[x])
                {
                    return true;
                }
            }
            return false;
        }
        private static bool UrlEndsWithNewLegacyExtensions(string url)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                if (url.EndsWith(StaticObjects.supportedLegacyExtenstion[x]))
                {
                    return true;
                }
            }
            return false;
        }
        private static bool UrlEndsWithLegacyExtensions(string url)
        {
            if (StaticObjects.supportedLegacyExtenstionold == null || !StaticObjects.supportedLegacyExtenstionold.Any())
                return false;
            return StaticObjects.supportedLegacyExtenstionold.Any(l => url.EndsWith("." + l));
        }

        private static string CleanUrlV3FromNewLegacyExtensions(string rawUrl)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
        }
        private static string CleanUrlV2FromNewLegacyExtensions(string rawUrl)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                if (rawUrl.Substring(rawUrl.Length - StaticObjects.supportedLegacyExtenstion[x].Length) == StaticObjects.supportedLegacyExtenstion[x])
                {
                    return rawUrl.Replace(StaticObjects.supportedLegacyExtenstion[x], string.Empty);
                }
            }
            return rawUrl;
        }
        private static string CleanUrlFromNewLegacyExtensions(string rawUrl)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                if (rawUrl.Contains(StaticObjects.supportedLegacyExtenstion[x]))
                {
                    return rawUrl.Replace(StaticObjects.supportedLegacyExtenstion[x], string.Empty);
                }
            }
            return rawUrl;
        }
        private static string CleanUrlFromLegacyExtensions(string rawurl)
        {
            if (StaticObjects.supportedLegacyExtenstion == null || !StaticObjects.supportedLegacyExtenstion.Any())
                return rawurl;

            var url = rawurl;
            StaticObjects.supportedLegacyExtenstion.ForEach(l => url = url.Replace("." + l, string.Empty));
            return url;
        }
        #endregion

        #region Method with unreachable code vs standard

        private static void TestMethodWithUnreachableCode()
        {
            string url = "http://www.sabado.pt/vida/detalhe/japao-constroi-casas-de-banho-de-genero-neutro?ref=HP_DestaquesSecundarios";
            Profiler.Compare(StandardCleanMethod, url).With(GarbagedMethod, StaticObjects.SimpleUrl);
        }
        private static string StandardCleanMethod(string rawUrl)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
        }
        private static string GarbagedMethod(string rawUrl)
        {
            int x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;

            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl; x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl; x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl; x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;
            x = -1;
            while (x++ < StaticObjects.supportedLegacyExtenstion.Count - 1)
            {
                var item = StaticObjects.supportedLegacyExtenstion[x];

                if (rawUrl.Length >= item.Length && rawUrl.Substring(rawUrl.Length - item.Length) == item)
                {
                    return rawUrl.Replace(item, string.Empty);
                }
            }
            return rawUrl;

        }

        #endregion



    }
}
