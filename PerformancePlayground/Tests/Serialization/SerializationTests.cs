﻿using Newtonsoft.Json;
using PerformancePlayground.TestObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Google;
using Google.Protobuf;

namespace PerformancePlayground.Tests.Serialization
{
    public static class SerializationTests
    {
        private static Wire.Serializer WireSerializer = new Wire.Serializer();

        private static Dictionary<string, string> dic;




        public static void Run()
        {
            Configure();

            RunAsserts();

            RunBenchmarks();

            TestEquality();

            TestJsonParseArray();

        }

        private static void Configure()
        {

            dic = new Dictionary<string, string>()
            {
                   { "0", "10"  },
                   { "1", "5"   },
                   { "2", "99"  },
                   { "3", "101" },
                   { "4", "110" },
                   { "5", "97"  },
                   { "6", "115" },
                   { "7", "34"  },
                   { "8", "8"   },
                   { "9", "77"  },
                   { "10", "89" },
                   { "11", "67"} ,
                   { "12", "111"},
                   { "13", "114"},
                   { "14", "114"},
                   { "15", "73" },
                   { "16", "68" }
            };
        }


        private static void RunAsserts()
        {
            var res = WireSerialization(StaticObjects.CustomTuple);

            Program.Profiler.Assert(WireSerialization, StaticObjects.CustomTuple).With(res);


            Program.Profiler.Assert(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
                   .With(WireDeserialization<string>, WireSerialization(StaticObjects.QuiteLargeString));


            Program.Profiler.Assert(ProtobufDeserialization<byte[]>, ProtobufSerialization(StaticObjects.ByteArray))
              .With(WireDeserialization<byte[]>, WireSerialization(StaticObjects.ByteArray));


            Program.Profiler.Assert(ProtobufDeserialization<List<int>>, ProtobufSerialization(StaticObjects.ListOfInt))
              .With(WireDeserialization<List<int>>, WireSerialization(StaticObjects.ListOfInt));


            Program.Profiler.Assert(ProtobufDeserialization<DateTime>, ProtobufSerialization(StaticObjects.CurrentDate))
              .With(WireDeserialization<DateTime>, WireSerialization(DateTime.Now));


            var msg = new Message
            {
                Text = StaticObjects.QuiteLargeString
            };

            Program.Profiler.Assert(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
                .With(NewProtobufDeserializationAssert, NewProtobufSerialization(msg));



        }

        public static void RunBenchmarks()
        {

            Program.Profiler.BenchmarkCompare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
            .With(WireDeserialization<string>, WireSerialization(StaticObjects.QuiteLargeString));

            Program.Profiler.BenchmarkCompare(ProtobufSerialization, StaticObjects.QuiteLargeString).With(ZeroSerialization, StaticObjects.QuiteLargeString);

            Program.Profiler.BenchmarkCompare(ProtobufSerialization, StaticObjects.DictionaryOfStringList)
            .With(ZeroSerialization, StaticObjects.DictionaryOfStringList);

            var msg = new Message
            {
                Text = StaticObjects.QuiteLargeString
            };

            Program.Profiler.Compare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
                .With(NewProtobufDeserialization, NewProtobufSerialization(msg));

        }

        public static void TestJsonParseArray()
        {
            Program.Profiler.Compare(JsonTryParseByteArray).With(JsonParseByteArray);
        }

        public static void TestSerialization()
        {
            TestWireFormatter();

            TestZeroFormatter();

            TestProtobufFormatter();
        }


        public static void TestWireFormatter()
        {


            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple)
            .With(WireSerialization, StaticObjects.CustomTuple);

            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple2)
            .With(WireSerialization, StaticObjects.CustomTuple2);

            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple3)
            .With(WireSerialization, StaticObjects.CustomTuple3);

            Program.Profiler.Compare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.SimpleUrl))
            .With(WireDeserialization<string>, WireSerialization(StaticObjects.SimpleUrl));

            Program.Profiler.Compare(ProtobufDeserialization<DateTime>, ProtobufSerialization(StaticObjects.CurrentDate))
            .With(WireDeserialization<DateTime>, WireSerialization(StaticObjects.CurrentDate));

            Program.Profiler.Compare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
            .With(WireDeserialization<string>, WireSerialization(StaticObjects.QuiteLargeString));

            Program.Profiler.Compare(ProtobufDeserialization<List<int>>, ProtobufSerialization(StaticObjects.ListOfInt))
            .With(WireDeserialization<List<int>>, WireSerialization(StaticObjects.ListOfInt));

            Program.Profiler.Compare(ProtobufDeserialization<Dictionary<int, string>>, ProtobufSerialization(StaticObjects.DictionaryOfString))
            .With(WireDeserialization<Dictionary<int, string>>, WireSerialization(StaticObjects.DictionaryOfString));

            Program.Profiler.Compare(ProtobufDeserialization<Dictionary<int, List<string>>>, ProtobufSerialization(StaticObjects.DictionaryOfStringList))
            .With(WireDeserialization<Dictionary<int, List<string>>>, WireSerialization(StaticObjects.DictionaryOfStringList));

            return;



        }


        public static void TestZeroFormatter()
        {

            #region wire vs zero

            Program.Profiler.Assert(WireSerialization, StaticObjects.CustomTuple).With(WireSerialization(StaticObjects.CustomTuple));

            Console.WriteLine(nameof(StaticObjects.CustomTuple));
            Program.Profiler.Compare(WireSerialization, StaticObjects.CustomTuple)
            .With(ZeroSerialization, StaticObjects.CustomTuple);

            Console.WriteLine(nameof(StaticObjects.CustomTuple2));
            Program.Profiler.Compare(WireSerialization, StaticObjects.CustomTuple2)
            .With(ZeroSerialization, StaticObjects.CustomTuple2);

           // Check bottom for explanation

           //Program.Profiler.Compare(WireSerialization, StaticObjects.CustomTuple3)
           //.With(ZeroSerialization, StaticObjects.CustomTuple3);

           Console.WriteLine(nameof(StaticObjects.SimpleUrl));
            Program.Profiler.Compare(WireDeserialization<string>, WireSerialization(StaticObjects.SimpleUrl))
            .With(ZeroDeserialization<string>, ZeroSerialization(StaticObjects.SimpleUrl));

            Console.WriteLine(nameof(StaticObjects.CurrentDate));
            Program.Profiler.Compare(WireDeserialization<DateTime>, WireSerialization(StaticObjects.CurrentDate))
            .With(ZeroDeserialization<DateTime>, ZeroSerialization(StaticObjects.CurrentDate));

            Console.WriteLine(nameof(StaticObjects.QuiteLargeString));
            Program.Profiler.Compare(WireDeserialization<string>, WireSerialization(StaticObjects.QuiteLargeString))
            .With(ZeroDeserialization<string>, ZeroSerialization(StaticObjects.QuiteLargeString));

            Console.WriteLine(nameof(StaticObjects.ListOfInt));
            Program.Profiler.Compare(WireDeserialization<List<int>>, WireSerialization(StaticObjects.ListOfInt))
            .With(ZeroDeserialization<List<int>>, ZeroSerialization(StaticObjects.ListOfInt));

            Console.WriteLine(nameof(StaticObjects.DictionaryOfString));
            Program.Profiler.Compare(WireDeserialization<Dictionary<int, string>>, WireSerialization(StaticObjects.DictionaryOfString))
            .With(ZeroDeserialization<Dictionary<int, string>>, ZeroSerialization(StaticObjects.DictionaryOfString));

            Console.WriteLine(nameof(StaticObjects.DictionaryOfStringList));
            Program.Profiler.Compare(WireDeserialization<Dictionary<int, List<string>>>, WireSerialization(StaticObjects.DictionaryOfStringList))
            .With(ZeroDeserialization<Dictionary<int, List<string>>>, ZeroSerialization(StaticObjects.DictionaryOfStringList));

            #endregion

            #region protobuf vs zero

            Console.WriteLine(nameof(StaticObjects.CustomTuple));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple)
            .With(ZeroSerialization, StaticObjects.CustomTuple);

            Console.WriteLine(nameof(StaticObjects.CustomTuple2));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple2)
            .With(ZeroSerialization, StaticObjects.CustomTuple2);

            // Check bottom for explanation
            //Program.Profiler.Compare(WireSerialization, StaticObjects.CustomTuple3)
            //.With(ZeroSerialization, StaticObjects.CustomTuple3);

            Console.WriteLine(nameof(StaticObjects.SimpleUrl));
            Program.Profiler.Compare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.SimpleUrl))
            .With(ZeroDeserialization<string>, ZeroSerialization(StaticObjects.SimpleUrl));

            Console.WriteLine(nameof(StaticObjects.CurrentDate));
            Program.Profiler.Compare(ProtobufDeserialization<DateTime>, ProtobufSerialization(StaticObjects.CurrentDate))
            .With(ZeroDeserialization<DateTime>, ZeroSerialization(StaticObjects.CurrentDate));

            Console.WriteLine(nameof(StaticObjects.QuiteLargeString));
            Program.Profiler.Compare(ProtobufDeserialization<string>, ProtobufSerialization(StaticObjects.QuiteLargeString))
            .With(ZeroDeserialization<string>, ZeroSerialization(StaticObjects.QuiteLargeString));

            Console.WriteLine(nameof(StaticObjects.ListOfInt));
            Program.Profiler.Compare(ProtobufDeserialization<List<int>>, ProtobufSerialization(StaticObjects.ListOfInt))
            .With(ZeroDeserialization<List<int>>, ZeroSerialization(StaticObjects.ListOfInt));

            Console.WriteLine(nameof(StaticObjects.DictionaryOfString));
            Program.Profiler.Compare(ProtobufDeserialization<Dictionary<int, string>>, ProtobufSerialization(StaticObjects.DictionaryOfString))
            .With(ZeroDeserialization<Dictionary<int, string>>, ZeroSerialization(StaticObjects.DictionaryOfString));

            Console.WriteLine(nameof(StaticObjects.DictionaryOfStringList));
            Program.Profiler.Compare(ProtobufDeserialization<Dictionary<int, List<string>>>, ProtobufSerialization(StaticObjects.DictionaryOfStringList))
            .With(ZeroDeserialization<Dictionary<int, List<string>>>, ZeroSerialization(StaticObjects.DictionaryOfStringList));

            #endregion










            //// Custom class
            //var settingsSerialized = ZeroFormatterSerializer.Serialize(Settings);
            //var settingsDeserialized = ZeroFormatterSerializer.Deserialize<ProfilerSettings>(settingsSerialized);
            //bool equals1 = CheckEqualityUsingJsonSerializer(settingsDeserialized, Settings);
            //Console.WriteLine($"{nameof(equals1)} = {equals1}");

            //// List<Int>
            //var ZeroListIntSerialized = ZeroFormatterSerializer.Serialize(StaticObjects.ListOfInt);
            //var ZeroListIntDeserialized = ZeroFormatterSerializer.Deserialize<List<int>>(ZeroListIntSerialized);
            //bool equals2 = CheckEqualityUsingJsonSerializer(ZeroListIntDeserialized, StaticObjects.ListOfInt);
            //Console.WriteLine($"{nameof(equals2)} = {equals2}");

            ////Large String 
            //var ZeroQuiteLargeStringSerialized = ZeroFormatterSerializer.Serialize(StaticObjects.QuiteLargeString);
            //var ZeroQuiteLargeStringDeserialized = ZeroFormatterSerializer.Deserialize<string>(ZeroQuiteLargeStringSerialized);
            //bool equals3 = CheckEqualityUsingJsonSerializer(ZeroQuiteLargeStringDeserialized, StaticObjects.QuiteLargeString);
            //Console.WriteLine($"{nameof(equals3)} = {equals3}");


            ////DateTime  
            //var ZeroDateTimeSerialized = ZeroFormatterSerializer.Serialize(StaticObjects.CurrentDate);
            //var ZeroDateTimeDeserialized = ZeroFormatterSerializer.Deserialize<DateTime>(ZeroDateTimeSerialized);
            // returns false because of newtonsoft deserialization of datetimes
            //// if we compare deserialized with original they are equal.
            //bool equals4 = CheckEqualityUsingJsonSerializer(ZeroDateTimeDeserialized, StaticObjects.CurrentDate);
            //Console.WriteLine($"{nameof(equals4)} = {equals4}");


            ////Custom Object 
            //var ZeroCustomTupleSerialized = ZeroFormatterSerializer.Serialize(StaticObjects.CustomTuple);
            //var ZeroCustomDeserialized = ZeroFormatterSerializer.Deserialize<Tuple<string, DateTime, decimal, int>>(ZeroCustomTupleSerialized);
            //bool equals5 = CheckEqualityUsingJsonSerializer(ZeroCustomDeserialized, StaticObjects.CustomTuple);
            //Console.WriteLine($"{nameof(equals5)} = {equals5}");


            ////another Custom Object
            //var ZeroAnotherTupleSerialized = ZeroFormatterSerializer.Serialize(StaticObjects.CustomTuple2);
            //var ZeroAnotherTupleCustomDeserialized = ZeroFormatterSerializer.Deserialize<Tuple<int?, decimal?, DateTime>>(ZeroAnotherTupleSerialized);
            //bool equals6 = CheckEqualityUsingJsonSerializer(ZeroAnotherTupleCustomDeserialized, StaticObjects.CustomTuple2);
            //Console.WriteLine($"{nameof(equals6)} = {equals6}");


            /*
              Errors 
              https://github.com/neuecc/ZeroFormatter
              -- Requires Registry because ZeroFormatter doesnt know which properties should serialize, thats why we need to add   [ZeroFormattable] and [Index(N)] Attributes to custom objects.
              -- ZeroFormatter also requires properties to be Virtual.
              -- ZeroFormatter also requires parameterless constructor.

              StringBuilder does not serialize
              Lazy does not serialize
              Object does not serialize
            */

            return;

        }



        public static void TestProtobufFormatter()
        {
            #region protobuf vs Json

            Console.WriteLine(nameof(StaticObjects.CustomTuple));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple)
            .With(JsonSerialization, StaticObjects.CustomTuple);

            Console.WriteLine(nameof(StaticObjects.CustomTuple2));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple2)
            .With(JsonSerialization, StaticObjects.CustomTuple2); ;

            Console.WriteLine(nameof(StaticObjects.SimpleUrl));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.SimpleUrl)
            .With(JsonSerialization, StaticObjects.SimpleUrl);

            Console.WriteLine(nameof(StaticObjects.QuiteLargeString));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.QuiteLargeString)
            .With(JsonSerialization, StaticObjects.QuiteLargeString);

            Console.WriteLine(nameof(StaticObjects.ListOfInt));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.ListOfInt)
            .With(JsonSerialization, StaticObjects.ListOfInt);

            Console.WriteLine(nameof(StaticObjects.DictionaryOfString));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.DictionaryOfString)
            .With(JsonSerialization, StaticObjects.DictionaryOfString);

            Console.WriteLine(nameof(StaticObjects.DictionaryOfStringList));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.DictionaryOfStringList)
            .With(JsonSerialization, StaticObjects.DictionaryOfStringList);

            #endregion


            #region protobuf vs Xml

            Console.WriteLine(nameof(StaticObjects.CustomTuple));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple)
            .With(XMLSerialization, StaticObjects.CustomTuple);

            Console.WriteLine(nameof(StaticObjects.CustomTuple2));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.CustomTuple2)
            .With(XMLSerialization, StaticObjects.CustomTuple2);

            Console.WriteLine(nameof(StaticObjects.SimpleUrl));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.SimpleUrl)
            .With(XMLSerialization, StaticObjects.SimpleUrl);

            Console.WriteLine(nameof(StaticObjects.QuiteLargeString));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.QuiteLargeString)
            .With(XMLSerialization, StaticObjects.QuiteLargeString);

            Console.WriteLine(nameof(StaticObjects.ListOfInt));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.ListOfInt)
            .With(XMLSerialization, StaticObjects.ListOfInt);

            Console.WriteLine(nameof(StaticObjects.DictionaryOfString));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.DictionaryOfString)
            .With(XMLSerialization, StaticObjects.DictionaryOfString);

            Console.WriteLine(nameof(StaticObjects.DictionaryOfStringList));
            Program.Profiler.Compare(ProtobufSerialization, StaticObjects.DictionaryOfStringList)
            .With(XMLSerialization, StaticObjects.DictionaryOfStringList);

            #endregion


            return;

        }


        private static byte[] ZeroSerialization<T>(T obj)
        {
            return ZeroFormatter.ZeroFormatterSerializer.Serialize(obj);
        }

        private static T ZeroDeserialization<T>(byte[] bytes)
        {
            return ZeroFormatter.ZeroFormatterSerializer.Deserialize<T>(bytes);
        }

        private static byte[] ProtobufSerialization<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(stream, obj);
                return stream.ToArray();
            }
        }
        private static T ProtobufDeserialization<T>(byte[] obj)
        {
            using (var stream = new MemoryStream(obj))
            {
                return ProtoBuf.Serializer.Deserialize<T>(stream);
            }
        }


        private static byte[] NewProtobufSerialization(Message msg)
        {
            return msg.ToByteArray();
        }

        private static Message NewProtobufDeserialization(byte[] obj)
        {
            var msg = new Message();
            msg.MergeFrom(obj);
            return msg;
        }
        private static string NewProtobufDeserializationAssert(byte[] obj)
        {
            return NewProtobufDeserialization(obj).Text;
        }

        private static byte[] WireSerialization<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                WireSerializer.Serialize(obj, stream);
                return stream.ToArray();
            }
        }
        private static T WireDeserialization<T>(byte[] obj)
        {
            T output;
            using (var ms = new MemoryStream(obj))
            {
                output = WireSerializer.Deserialize<T>(ms);
            }

            return output;
        }

        private static string XMLSerialization<T>(T obj)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));

            using (var sWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sWriter))
                {
                    xsSubmit.Serialize(writer, obj);
                    return sWriter.ToString(); // Your XML
                }
            }
        }

        private static T XMLDeserialization<T>(string str)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (TextReader reader = new StringReader(str))
            {
                return (T)ser.Deserialize(reader);
            }
        }

        private static string JsonSerialization(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        private static T JsonDeserializationFromString<T>(string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }

        private static T JsonDeserialization<T>(byte[] bytes)
        {
            return JsonConvert.DeserializeObject<T>(Encoding.ASCII.GetString(bytes));
        }

        private static byte[] JsonSerializationToByteArray(object obj)
        {
            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(obj));
        }

        private static void TestEquality()
        {
            var res1 = new StringBuilder("Test");
            var res2 = new StringBuilder("Test");

            Program.Profiler.Test(CheckEqualityUsingJsonSerializer, res1, res2);

            var result = CheckEqualityUsingJsonSerializer(res1, res2);
        }
        public static bool CheckEqualityUsingJsonSerializer(object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            if (obj.GetType() != another.GetType()) return false;
            JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var objJson = JsonConvert.SerializeObject(obj, microsoftDateFormatSettings);
            var anotherJson = JsonConvert.SerializeObject(another, microsoftDateFormatSettings);

            return objJson == anotherJson;
        }

        public static byte[] JsonTryParseByteArray()
        {
            byte res;
            byte[] byteArray = new byte[dic.Count];

            foreach (var x in dic)
            {
                byte.TryParse(x.Value, out res);
                byteArray.Append(res);
            };

            return byteArray;
        }


        public static byte[] JsonParseByteArray()
        {
            var byteArray = dic.Select(x => byte.Parse(x.Value)).ToArray();
            return byteArray;
        }

        public static byte[] JsonConvertByteArray()
        {
            var byteArray = dic.Select(x => Convert.ToByte(x.Value)).ToArray();

            return byteArray;

        }

    }
}