﻿using PerformancePlayground.Tests;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PerformancePlayground.Tests.Async
{
    public static class AsyncTests
    {

        public async static void Run()
        {
             TestAsyncNullChecksPrecedence();

             TestAsyncPrecedence();
             
        }

        #region TestNullChecksAfterAsyncCalls
        private async static void TestAsyncNullChecksPrecedence()
        {
            //await RunNullCheckOnAwaitedCallResult();
            //Console.WriteLine("");
            //await RunNullCheckOnCallResult();
        }
        private static Task RunNullCheckOnAwaitedCallResult()
        {
            return Task.Run(async () =>
               {
                   try
                   {
                       Console.WriteLine($"Starting {nameof(RunNullCheckOnAwaitedCallResult)}");

                       var obj = await GetNullObjectAsTask(1000, nameof(RunNullCheckOnAwaitedCallResult) + " - Executing Async Task").ConfigureAwait(false);

                       if (obj == null)
                       {
                           Console.WriteLine(nameof(RunNullCheckOnAwaitedCallResult) + " - Returned on NullCheck");
                           return;
                       }
                       var stuff = new
                       {
                           body = obj,
                           a = 1,
                           b = 2,
                           c = 3,
                           d = "teste"
                       };

                       Console.WriteLine(nameof(RunNullCheckOnAwaitedCallResult) + " - Writing obj.body");
                       Console.WriteLine(stuff.body);

                       Console.WriteLine(nameof(RunNullCheckOnAwaitedCallResult) + " - Waiting");
                       Thread.Sleep(2000);


                       Console.WriteLine(nameof(RunNullCheckOnAwaitedCallResult) + " - Writing obj.body.Prop");
                       Console.WriteLine(((dynamic)stuff.body).Prop);

                   }

                   catch (Exception ex)
                   {
                       // Get stack trace for the exception with source file information
                       var st = new StackTrace(ex, true);
                       // Get the top stack frame
                       var frame = st.GetFrame(0);
                       // Get the line number from the stack frame
                       var line = frame.GetFileLineNumber();

                       Console.WriteLine(nameof(RunNullCheckOnCallResult) + $" - error at line {GetLineNumberFromException(ex)}. Finished.");
                   }
                   finally {
                       Console.WriteLine(nameof(RunNullCheckOnAwaitedCallResult) + " - Finished.");
                   }
               });
        }

        private static Task RunNullCheckOnCallResult()
        {
            return Task.Run(async () =>
            {
                try
                {
                    Console.WriteLine($"Starting {nameof(RunNullCheckOnCallResult)}");

                    var obj = GetNullObjectAsTask(1000, nameof(RunNullCheckOnCallResult) + " - Executing Async Task");

                    if (obj == null)
                    {
                        Console.WriteLine(nameof(RunNullCheckOnCallResult) + " - Returned on NullCheck");
                        return;
                    }
                    var stuff = new
                    {
                        body = obj,
                        a = 1,
                        b = 2,
                        c = 3,
                        d = "teste"
                    };

                    Console.WriteLine(nameof(RunNullCheckOnCallResult) + " - Writing obj.body");
                    Console.WriteLine(stuff.body);

                    Console.WriteLine(nameof(RunNullCheckOnCallResult) + " - Waiting");
                    Thread.Sleep(2000);

                    Console.WriteLine(nameof(RunNullCheckOnCallResult) + " - Writing obj.body.Prop");
                    Console.WriteLine(((dynamic)stuff.body).Prop);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(nameof(RunNullCheckOnCallResult) + $" - error at line {GetLineNumberFromException(ex)}. Finished.");
                }
                finally
                {
                    Console.WriteLine(nameof(RunNullCheckOnCallResult) + " - Finished.");
                }
            });
        }

        private static Task<object> GetNullObjectAsTask(int miliseconds, string msg)
        {
            return Task.Run(() =>
            {
                Console.WriteLine(msg);
                Thread.Sleep(miliseconds);
                return Task.FromResult<object>(null);
            });
        }

        private static int GetLineNumberFromException(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }

        #endregion


        #region TestAsyncPrecedence
        private async static void TestAsyncPrecedence()
        {
            //RunAwaitedAsync();
            //RunNotAwaitedAsync();
        }
        private async static void RunAwaitedAsync()
        {
            await Sleep(1000, nameof(RunAwaitedAsync) + " - 1").ConfigureAwait(false);
            await Sleep(1000, nameof(RunAwaitedAsync) + " - 2").ConfigureAwait(false);
            await Sleep(1000, nameof(RunAwaitedAsync) + " - 3").ConfigureAwait(false);
            await Sleep(1000, nameof(RunAwaitedAsync) + " - 4").ConfigureAwait(false);
            await Sleep(1000, nameof(RunAwaitedAsync) + " - 5").ConfigureAwait(false);
        }
        private async static void RunNotAwaitedAsync()
        {
            Sleep(1000, nameof(RunNotAwaitedAsync) + " - 1");
            Sleep(1000, nameof(RunNotAwaitedAsync) + " - 2");
            Sleep(1000, nameof(RunNotAwaitedAsync) + " - 3");
            Sleep(1000, nameof(RunNotAwaitedAsync) + " - 4");
            Sleep(1000, nameof(RunNotAwaitedAsync) + " - 5");
        }

        private static Task Sleep(int miliseconds, string msg)
        {
            return Task.Run(() =>
            {
                Thread.Sleep(miliseconds);
                Console.WriteLine(msg);
            });
        }
        #endregion
    }
}
