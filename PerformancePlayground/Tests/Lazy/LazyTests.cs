﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PerformancePlayground.Tests.Lazy
{
    public static class LazyTests
    {
        static Lazy<string> lazyMessage => new Lazy<string>(() =>
           {
               Thread.Sleep(100);
               return "a";
           });

        public static void Run()
        {
            TestLazyInitialization();
        }


        private static void TestLazyInitialization()
        {
            Program.Profiler.Test(LazySerializedMessage, lazyMessage);
        }

        private static string LazySerializedMessage(Lazy<string> lazyMessage)
        {
            return lazyMessage.Value;
        }

    }
}
