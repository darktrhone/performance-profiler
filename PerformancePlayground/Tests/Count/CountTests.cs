﻿using PerformancePlayground.TestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PerformancePlayground.Tests.Count
{
    public static class CountTests
    {
        public static void Run()
        {
            TestCounts();
        }
        private static void TestCounts()
        {
            Program.Profiler.Test(UsingCountProperty, StaticObjects.ListOfInt);
            Program.Profiler.Test(UsingCountMethod, StaticObjects.ListOfInt);
            Program.Profiler.Test(UsingAnyMethod, StaticObjects.ListOfInt);
            Program.Profiler.Test(UsingCountProperty, StaticObjects.ListOfInt);
            Program.Profiler.Compare(UsingCountProperty, StaticObjects.ListOfInt).With(UsingAnyMethod, StaticObjects.ListOfInt);
            Program.Profiler.Assert(UsingCountProperty, StaticObjects.ListOfInt).With(UsingCountMethod, StaticObjects.ListOfInt);
            Program.Profiler.Assert(UsingCountProperty, StaticObjects.ListOfInt).With(1500);
            Program.Profiler.Test(UsingCountMethod, StaticObjects.ListOfInt);
            Program.Profiler.Test(UsingAnyMethod, StaticObjects.ListOfInt);
        }

        private static int UsingCountProperty(List<int> list)
        {
            return list.Count;
        }
        private static int UsingCountMethod(List<int> list)
        {
            return list.Count();
        }
        private static bool UsingAnyMethod(List<int> list)
        {
            return list.Any();
        }

    }
}
