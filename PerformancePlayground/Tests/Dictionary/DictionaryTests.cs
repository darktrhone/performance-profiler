﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace PerformancePlayground.Tests.Collections
{
    public static class DictionaryTests
    {
        static Dictionary<string, string> rawDictionary = new Dictionary<string, string>();
        static Dictionary<string, string> dic1 = new Dictionary<string, string>();
        static Dictionary<string, string> dic2 = new Dictionary<string, string>();
        static Dictionary<string, string> dic3 = new Dictionary<string, string>();

        public static void Run()
        {
            ConfigureDictionary();

            CheckEqualityComparer();

            CheckAllocations();

            CompareAccessPerformance();
        }

        private static void ConfigureDictionary()
        {
            rawDictionary.Add("Key", "Value");
            rawDictionary.Add("KEY-1", "a");
            rawDictionary.Add("KEy-2", "b");
            rawDictionary.Add("key-3", "c");
            rawDictionary.Add("kEY-4", "d");
            rawDictionary.Add("KeY-5", "e");
            rawDictionary.Add("KEY-6", "A");
            rawDictionary.Add("KEy-7", "B");
            rawDictionary.Add("key-8", "C");
            rawDictionary.Add("kEY-9", "D");
            rawDictionary.Add("KeY-0", "E");
        }
        private static void CheckAllocations()
        {
            Program.Profiler.Compare(GetNewDictionary).With(GetExistingNewDictionary);
            Program.Profiler.BenchmarkCompare(GetNewDictionary).With(GetExistingNewDictionary);
        }

        private static Dictionary<string, string> GetNewDictionary()
        {
            return new Dictionary<string, string>(rawDictionary.ToDictionary(q => q.Key, q => q.Value), StringComparer.OrdinalIgnoreCase);
        }
        private static Dictionary<string, string> GetExistingNewDictionary()
        {
            return rawDictionary.ToDictionary(q => q.Key, q => q.Value, StringComparer.OrdinalIgnoreCase);
        }


        private static void CompareAccessPerformance()
        {
            Program.Profiler.Compare(TryGetValueFromDictionary).With(ContainsAndReturnValue);
        }


        private static void CheckEqualityComparer()
        {
            dic1 = new Dictionary<string, string>(rawDictionary.ToDictionary(q => q.Key, q => q.Value), StringComparer.OrdinalIgnoreCase);
            dic2 = rawDictionary.ToDictionary(q => q.Key, q => q.Value, StringComparer.OrdinalIgnoreCase);
            dic3 = rawDictionary.ToDictionary(q => q.Key, q => q.Value);

            Program.Profiler.Assert(GetExistingValueFromDictionaryWithEquality).With(GetNotExistingValueFromDictionaryWithEquality);
            Program.Profiler.Compare(GetExistingValueFromDictionaryWithEquality).With(GetNotExistingValueFromDictionaryWithEquality);

            Program.Profiler.Assert(GetExistingValueFromDictionaryWithEquality2).With(GetNotExistingValueFromDictionaryWithEquality2);
            Program.Profiler.Compare(GetExistingValueFromDictionaryWithEquality2).With(GetNotExistingValueFromDictionaryWithEquality2);

            Program.Profiler.Assert(GetExistingValueFromDictionaryWithEquality3).With(GetNotExistingValueFromDictionaryWithEquality3);
            Program.Profiler.Compare(GetExistingValueFromDictionaryWithEquality3).With(GetNotExistingValueFromDictionaryWithEquality3);

        }

        private static string GetExistingValueFromDictionaryWithEquality()
        {
            return dic1["KEY-1"];
        }
        private static string GetNotExistingValueFromDictionaryWithEquality()
        {
            return dic1["key-1"];
        }

        private static string GetExistingValueFromDictionaryWithEquality2()
        {
            return dic2["KEY-1"];
        }
        private static string GetNotExistingValueFromDictionaryWithEquality2()
        {
            return dic2["key-1"];
        }

        private static string GetExistingValueFromDictionaryWithEquality3()
        {
            dic3.TryGetValue("KEY-1", out var x);
            return x;
        }
        private static string GetNotExistingValueFromDictionaryWithEquality3()
        {
            dic3.TryGetValue("key-1", out var x);
            return x;
        }

        private static string TryGetValueFromDictionary()
        {
            rawDictionary.TryGetValue("KeyXPTO", out var x);
            return x;
        }

        private static string ContainsAndReturnValue()
        {
            string x = null;
            if (rawDictionary.ContainsKey("KeyXPTO"))
            {
                x = rawDictionary["KeyXPTO"];
            }

            return x;
        }
    }
}
