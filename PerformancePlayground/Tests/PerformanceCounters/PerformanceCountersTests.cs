﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace PerformancePlayground.Tests.PerformanceCountersTests
{
    public static class PerformanceCountersTests
    {
        public static void Run()
        {
            TestPerformanceCounters();
        }

        private static void TestPerformanceCounters()
        {
            ReadValuesOfPerformanceCounters();
        }

        private static void ReadValuesOfPerformanceCounters()
        {
            PerformanceCounter processorTimeCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            PerformanceCounter memoryUsage = new PerformanceCounter("Memory", "Available MBytes");
            Console.WriteLine("CPU usage counter: ");
            Console.WriteLine("Category: {0}", processorTimeCounter.CategoryName);
            Console.WriteLine("Instance: {0}", processorTimeCounter.InstanceName);
            Console.WriteLine("Counter name: {0}", processorTimeCounter.CounterName);
            Console.WriteLine("Help text: {0}", processorTimeCounter.CounterHelp);
            Console.WriteLine("------------------------------");
            Console.WriteLine("Memory usage counter: ");
            Console.WriteLine("Category: {0}", memoryUsage.CategoryName);
            Console.WriteLine("Counter name: {0}", memoryUsage.CounterName);
            Console.WriteLine("Help text: {0}", memoryUsage.CounterHelp);
            Console.WriteLine("------------------------------");
            while (true)
            {
                Console.WriteLine("CPU: {0} Free Ram: {1}GB", processorTimeCounter.NextValue(), memoryUsage.NextValue());
                Thread.Sleep(200);
            }
        }
    }
}
