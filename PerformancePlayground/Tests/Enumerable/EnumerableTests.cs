﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PerformancePlayground.Tests.Collections
{
    using System.Collections;

    public static class EnumerableTests
    {
        private static int[] array = new int [20];
        static List<int> list = new List<int>();
        static ArrayList arrayList = new ArrayList();

        private static IEnumerable<Func<int>> smallIntIEnumerable;
        private static IEnumerable<Func<int>> bigIntIEnumerable;

        private static Func<int> GetInt(int i)
        {
            return () =>
            {
                Console.WriteLine(i);
                return i;
            };
        }
    
        public static void Run()
        {
           var asmallIntIEnumerable = new List<Func<int>>();
            var abigIntIEnumerable = new List<Func<int>>();
            for (int i = 0; i < 10; i++)
            {
                asmallIntIEnumerable.Add(GetInt(i));
            }
            for (int i = 0; i < 100; i++)
            {
                abigIntIEnumerable.Add(GetInt(i));
            }

            smallIntIEnumerable = asmallIntIEnumerable.AsEnumerable();
            bigIntIEnumerable = abigIntIEnumerable.AsEnumerable();

            // var resultsmall = smallIntIEnumerable.Where(x => x() > 0).Select(x => x()).Take(8);
            
            var resultbig = bigIntIEnumerable.Where(x => true).Select(x => x()).Take(8);


            // var zz = resultsmall.ToList();
            var xx = resultbig.ToList();

            xx.Count();
 

        }
 
 
    }
}
