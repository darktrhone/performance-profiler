﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PerformancePlayground.Tests.Tuples
{
    public static class ExtensionInstanceTest
    {
        public static void Run()
        {
            var brand = new Brand()
            {
                Name = "Gucci",
                CultureCode = "ab-ab",
                ResourceId = 1,
                TenantId = 20000,
                LastUpdate = DateTimeOffset.Now
            };
            
            var brand2 = new Brand2()
            {
                Name = "Gucci",
                CultureCode = "ab-ab",
                ResourceId = 1,
                TenantId = 20000,
                LastUpdate = DateTimeOffset.Now
            };
            
            
            
            Program.Profiler.Compare(brand2.GetCacheKeyStatic).With(brand.GetCacheKeyInstance);
            
            Program.Profiler.Compare(brand2.GetCacheKeyStatic).With(brand.GetCacheKeyInstance);
        }
    }
    
    
    public abstract class BaseEntity
    {
        public int TenantId { get; set; }

        public int ResourceId { get; set; }

        public string CultureCode { get; set; }

        public string Name { get; set; }

        public System.DateTimeOffset LastUpdate { get; set; }
        
        public abstract  string GetCacheKeyInstance();
    }
    
    
    public  class Brand : BaseEntity
    {
        public override string GetCacheKeyInstance()
        {
            return ($"{nameof(Brand)}|{TenantId}|{CultureCode}|{ResourceId}|{Name}");
        }
    }
    public abstract class BaseEntity2
    {
        public int TenantId { get; set; }

        public int ResourceId { get; set; }

        public string CultureCode { get; set; }

        public string Name { get; set; }

        public System.DateTimeOffset LastUpdate { get; set; }
    }
    public  class Brand2 : BaseEntity2
    {
    }
    
    public static class BrandExtension
    {
        public static string GetCacheKeyStatic(this Brand2 obj)
        {
            return ($"{nameof(Brand2)}|{obj.TenantId}|{obj.CultureCode}|{obj.ResourceId}|{obj.Name}");
        }
    }
}
