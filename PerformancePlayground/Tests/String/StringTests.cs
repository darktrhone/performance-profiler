﻿using PerformancePlayground.TestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace PerformancePlayground.Tests.String
{
    public static class StringTests
    {
        private static Random Random = new Random();

        public static void Run()
        {
            TestLog();

            //TestStringGenerator();

            //TestStringAttribution();

            //TestStringsConcatenation();

            //TestStringSearch();

            //TestRegex();

            //TestXmlRead();

            //TestCompares();

            //TestSpanAllocations();

            //BenchmarkStringConcat();
        }


        private static void TestLog()
        {

            for(var i = 100; i < 10000; i++)
            {
                Console.WriteLine(Math.Pow(5, Math.Log(i) * 1.15));

                   

            }

        }

        private static void TestStringGenerator()
        {
            var xy = RandomFiveLetterString();
            var ya = RandomString();

            for (int i = 0; i < 10; i++)
            {
                var x = RandomFiveLetterString();
                var y = RandomString();
                continue;
            }

            Program.Profiler.Compare(RandomFiveLetterString).With(RandomString);
        }

        public static string RandomFiveLetterString() => string.Format("{0}{1}{2}{3}{4}", RandomChar, RandomChar, RandomChar, RandomChar, RandomChar);

        private static char RandomChar => (char)Random.Next('a', 'z');

        public static string RandomString() => new string(Enumerable.Repeat("abcdefghijklmnopqrstuvwxyz", 5).Select(s => s[Random.Next(s.Length)]).ToArray());



        private static void TestSpanAllocations()
        {
            Program.Profiler.Compare(AllocateStringToString).With(AllocateSpanToString);
        }

        private static string AllocateStringToString()
        {
            StaticObjects.ClassSample.Value = StaticObjects.AnotherClassSample.Value;

            return StaticObjects.ClassSample.Value;
        }
        private static string AllocateSpanToString()
        {
            StaticObjects.ClassSample.AnotherValue = StaticObjects.AnotherClassSample.AnotherValue.AsSpan().ToString();

            return StaticObjects.ClassSample.AnotherValue;
        }


        private static string BenchmarkStringConcat()
        {
            var res = "";

            Parallel.For(0, 50, _ => res += StaticObjects.QuiteLargeString);

            return res;
        }

        private static void TestCompares()
        {
            RegexOptions RegexOpts = RegexOptions.IgnoreCase | RegexOptions.Compiled;
            string regexPattern = "Earth's only|Moon";
            string earthOnlyString = "Earth's only";

            Program.Profiler.Compare(Contains, StaticObjects.QuiteLargeString, earthOnlyString).With(IndexOf, StaticObjects.QuiteLargeString, earthOnlyString);
            Program.Profiler.Compare(Contains, StaticObjects.QuiteLargeString, earthOnlyString).With(RegexIsMatch, StaticObjects.QuiteLargeString, regexPattern, RegexOpts);
            Program.Profiler.Compare(IndexOfOrdinal, StaticObjects.QuiteLargeString, earthOnlyString).With(IndexOfOrdinalIgnoreCase, StaticObjects.QuiteLargeString, earthOnlyString);
        }


        #region Test XML Read
        private static void TestXmlRead()
        {
            Program.Profiler.Test(XMLRead);
        }

        private static void XMLRead()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(StaticObjects.Xml);
        }

        #endregion

        #region Test String Attribution
        private static void TestStringAttribution()
        {
            Program.Profiler.Compare(StringAttribution).With(StringWithoutAttribution);
        }
        private static void StringAttribution()
        {
            string x = AttributeString();
        }
        private static void StringWithoutAttribution()
        {
            AttributeString();
        }
        private static string AttributeString()
        {
            return StaticObjects.Xml;
        }
        #endregion

        #region Test String IndexOf and Contains and Regex Match

        private static void TestStringSearch()
        {
            string url = "http://www.sabado.pt/vida/detalhe/japao-constroi-casas-de-banho-de-genero-neutro?ref=HP_DestaquesSecundarios";
            string bots = @"ABCdatos|BotLink|Acme.Spider|Ahoy!|The Homepage Finder";
            string text = StaticObjects.QuiteLargeString;
            string value = "BotLink";
            string value2 = "NASA Apollo";
            RegexOptions RegexOpts = RegexOptions.IgnoreCase | RegexOptions.Compiled;

            Program.Profiler.Compare(Contains, text, value2).With(IndexOf, text, value2);
            Program.Profiler.Compare(Contains, text, value2).With(RegexIsMatch, value2, text, RegexOpts);
            Program.Profiler.Compare(IndexOfOrdinal, text, value2).With(IndexOfOrdinalIgnoreCase, text, value2);
            Program.Profiler.Compare(IndexOfOrdinal, text, value2).With(Contains, text, value2);
            Program.Profiler.Test(IndexOf, url, "tomate");
            Program.Profiler.Test(Contains, url, "tomate");


        }
        private static bool RegexIsMatch(string value, string pattern, RegexOptions opts)
        {
            return Regex.IsMatch(value, pattern, opts);
        }
        private static bool IndexOfOrdinal(string baseString, string value)
        {
            return baseString.IndexOf(value, StringComparison.Ordinal) > -1;
        }
        private static bool IndexOfOrdinalIgnoreCase(string baseString, string value)
        {
            return baseString.IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
        }
        private static bool IndexOf(string baseString, string value)
        {
            return baseString.IndexOf(value) > -1;
        }
        private static bool Contains(string baseString, string value)
        {
            return baseString.Contains(value);
        }
        #endregion

        #region Test String Concatenation

        private static void TestStringsConcatenation()
        {
            string a = "O Lorem Ipsum é um texto modelo da indústria tipográfica e de impressão. O Lorem Ipsum tem vindo a";
            string b = "ser o texto padrão usado por estas indústrias desde o ano de 1500 quando uma misturou os caracteres de um texto ";
            string c = "para criar um espécime de livro. Este texto não só sobreviveu 5 séculos, mas também o salto para ";
            string d = "a tipografia electrónica, mantendo-se t, que continham passagens com Lorem Ipsum, e mais recentemente com os programas de publicação como o";
            string e = "Aldus PageMaker que incluem versões do Lorem Ipsum.";
            var len = (a + b + c + d + e).Length;

            Program.Profiler.Compare(StringBuilderWithNoCapacity, a, b, c, d, e).With(StringBuilderWithExactCapacity, a, b, c, d, e, len + 1);
            Program.Profiler.Compare(StringBuilderWithExactCapacity, a, b, c, d, e, len + 1).With(StringPlusOperator, a, b, c, d, e);
            Program.Profiler.Assert(StringBuilderWithExactCapacity, a, b, c, d, e, len + 1).With(StringPlusOperator, a, b, c, d, e);
            Program.Profiler.Test(StringBuilderWithExactCapacity, a, b, c, d, e, len + 1);
            Program.Profiler.Test(StringBuilderWith10000Capacity, a, b, c, d, e);
            Program.Profiler.Test(StringBuilderWith5000Capacity, a, b, c, d, e);
            Program.Profiler.Test(StringBuilderWith1000Capacity, a, b, c, d, e);
            Program.Profiler.Test(StringBuilderWith100Capacity, a, b, c, d, e);
            Program.Profiler.Test(StringBuilderWithNoCapacity, a, b, c, d, e);
            Program.Profiler.Test(StringBuild, a, b, c, d, e);
            Program.Profiler.Test(StringFormat, a, b, c, d, e);
            Program.Profiler.Test(StringInterpolation, a, b, c, d, e);
            Program.Profiler.Test(StringPlusOperator, a, b, c, d, e);

        }

        private static string StringBuilderWith10000Capacity(string a, string b, string c, string d, string e)
        {
            StringBuilder sb = new StringBuilder(10000);
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringBuilderWith5000Capacity(string a, string b, string c, string d, string e)
        {
            StringBuilder sb = new StringBuilder(5000);
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringBuilderWith1000Capacity(string a, string b, string c, string d, string e)
        {
            StringBuilder sb = new StringBuilder(1000);
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringBuilderWith100Capacity(string a, string b, string c, string d, string e)
        {
            StringBuilder sb = new StringBuilder(100);
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringBuilderWithNoCapacity(string a, string b, string c, string d, string e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringBuilderWithExactCapacity(string a, string b, string c, string d, string e, int cap)
        {
            StringBuilder sb = new StringBuilder(cap);
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringBuild(string a, string b, string c, string d, string e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}{1}{2}{3}{4}", a, b, c, d, e);
            return sb.ToString();
        }
        private static string StringFormat(string a, string b, string c, string d, string e)
        {
            return string.Format("{0}{1}{2}{3}{4}", a, b, c, d, e);
        }
        private static string StringInterpolation(string a, string b, string c, string d, string e)
        {
            return $"{a}{b}{c}{d}{e}";
        }
        private static string StringPlusOperator(string a, string b, string c, string d, string e)
        {
            return a + b + c + d + e;
        }
        #endregion

        #region Test Regex
        private static void TestRegex()
        {
            RegexTimeout();
        }

        private static void RegexTimeout()
        {
            try
            {
                Regex re = new Regex("(x+x+)+y", RegexOptions.None, TimeSpan.FromMilliseconds(250));

                string success = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxy";
                string failure = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

                re.Match(success);
                re.Match(failure);

            }
            catch (RegexMatchTimeoutException ex)
            {
                Console.WriteLine("Match timed out!");
                Console.WriteLine("Applied Default: " + ex.MatchTimeout);
            }
            catch (Exception e)
            {
                Console.WriteLine("Match timed out!");
                return;
            }
        }
        #endregion
    }
}
