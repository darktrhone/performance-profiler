﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PerformancePlayground.Tests.Collections
{
    using System.Collections;

    public static class ListTests
    {
        private static int[] array = new int [20];
        static List<int> list = new List<int>();
        static ArrayList arrayList = new ArrayList();

        private static IEnumerable<long> brandList = new long[] { 1, 2 };
        private static IEnumerable<long> wishList = new long[] { 5, 6, 7, 8, 1, 9, 4, 3, 2 };

        public static void Run()
        {
            // AddValues();

            SingleEnumerationVsMultipleEnumeration();
        }

        private static void SingleEnumerationVsMultipleEnumeration()
        {
            
            Program.Profiler.Compare(TraverseSingleEnumerable).With(TraverseMultipleEnumerable);
            
             
        }

        private static void TraverseMultipleEnumerable()
        {
            foreach (var value in wishList)
            {
                if (brandList.Contains(value))
                {
                    var i = value;
                }
            }

        }

        private static void TraverseSingleEnumerable()
        {
            foreach (var value in brandList.Where(x => brandList.Contains(x)))
            {
                var i = value;
            }

        }


        private static void AddValues()
        {
            AddListValue();
            AddArrayValue();
            AddArrayListValue();

            Program.Profiler.Compare(CountListValue).With(CountArrayValue);

            Program.Profiler.Compare(CountListValue).With(CountArrayListValue);
        }

        private static void AddListValue()
        {
            for (int i = 0; i < 20; i++)
            {
                list.Add(20);
            }
        }

        private static void AddArrayValue()
        {
            for (int i = 0; i < 20; i++)
            {
                array[i] = 20;
            }
        }


        private static void CountListValue()
        {
            int total = 0;
            foreach (var x in list)
            {
                total += x;
            }
        }

        private static void CountArrayValue()
        {
            int total = 0;
            foreach (var x in array)
            {
                total += x;
            }
        }

        private static void CountArrayListValue()
        {
            int total = 0;
            foreach (int x in arrayList)
            {
                total += x;
            }
        }


        private static void AddArrayListValue()
        {
            for (int i = 0; i < 20; i++)
            {
                arrayList.Add(20);
            }
        }
    }
}
